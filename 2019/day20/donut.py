#!/usr/bin/env python3

'''--- Day 20: Donut Maze ---

You notice a strange pattern on the surface of Pluto and land nearby
to get a closer look. Upon closer inspection, you realize you've come
across one of the famous space-warping mazes of the long-lost Pluto
civilization!

Because there isn't much space on Pluto, the civilization that used to
live here thrived by inventing a method for folding
spacetime. Although the technology is no longer understood, mazes like
this one provide a small glimpse into the daily life of an ancient
Pluto citizen.

This maze is shaped like a donut. Portals along the inner and outer
edge of the donut can instantly teleport you from one side to the
other. For example:

         A           
         A           
  #######.#########  
  #######.........#  
  #######.#######.#  
  #######.#######.#  
  #######.#######.#  
  #####  B    ###.#  
BC...##  C    ###.#  
  ##.##       ###.#  
  ##...DE  F  ###.#  
  #####    G  ###.#  
  #########.#####.#  
DE..#######...###.#  
  #.#########.###.#  
FG..#########.....#  
  ###########.#####  
             Z       
             Z       

This map of the maze shows solid walls (#) and open passages
(.). Every maze on Pluto has a start (the open tile next to AA) and an
end (the open tile next to ZZ). Mazes on Pluto also have portals; this
maze has three pairs of portals: BC, DE, and FG. When on an open tile
next to one of these labels, a single step can take you to the other
tile with the same label. (You can only walk on . tiles; labels and
empty space are not traversable.)

One path through the maze doesn't require any portals. Starting at AA,
you could go down 1, right 8, down 12, left 4, and down 1 to reach ZZ,
a total of 26 steps.

However, there is a shorter path: You could walk from AA to the inner
BC portal (4 steps), warp to the outer BC portal (1 step), walk to the
inner DE (6 steps), warp to the outer DE (1 step), walk to the outer
FG (4 steps), warp to the inner FG (1 step), and finally walk to ZZ (6
steps). In total, this is only 23 steps.

Here is a larger example:

                   A               
                   A               
  #################.#############  
  #.#...#...................#.#.#  
  #.#.#.###.###.###.#########.#.#  
  #.#.#.......#...#.....#.#.#...#  
  #.#########.###.#####.#.#.###.#  
  #.............#.#.....#.......#  
  ###.###########.###.#####.#.#.#  
  #.....#        A   C    #.#.#.#  
  #######        S   P    #####.#  
  #.#...#                 #......VT
  #.#.#.#                 #.#####  
  #...#.#               YN....#.#  
  #.###.#                 #####.#  
DI....#.#                 #.....#  
  #####.#                 #.###.#  
ZZ......#               QG....#..AS
  ###.###                 #######  
JO..#.#.#                 #.....#  
  #.#.#.#                 ###.#.#  
  #...#..DI             BU....#..LF
  #####.#                 #.#####  
YN......#               VT..#....QG
  #.###.#                 #.###.#  
  #.#...#                 #.....#  
  ###.###    J L     J    #.#.###  
  #.....#    O F     P    #.#...#  
  #.###.#####.#.#####.#####.###.#  
  #...#.#.#...#.....#.....#.#...#  
  #.#####.###.###.#.#.#########.#  
  #...#.#.....#...#.#.#.#.....#.#  
  #.###.#####.###.###.#.#.#######  
  #.#.........#...#.............#  
  #########.###.###.#############  
           B   J   C               
           U   P   P               

Here, AA has no direct path to ZZ, but it does connect to AS and
CP. By passing through AS, QG, BU, and JO, you can reach ZZ in 58
steps.

In your maze, how many steps does it take to get from the open tile
marked AA to the open tile marked ZZ?

Your puzzle answer was 454.
--- Part Two ---

Strangely, the exit isn't open when you reach it. Then, you remember:
the ancient Plutonians were famous for building recursive spaces.

The marked connections in the maze aren't portals: they physically
connect to a larger or smaller copy of the maze. Specifically, the
labeled tiles around the inside edge actually connect to a smaller
copy of the same maze, and the smaller copy's inner labeled tiles
connect to yet a smaller copy, and so on.

When you enter the maze, you are at the outermost level; when at the
outermost level, only the outer labels AA and ZZ function (as the
start and end, respectively); all other outer labeled tiles are
effectively walls. At any other level, AA and ZZ count as walls, but
the other outer labeled tiles bring you one level outward.

Your goal is to find a path through the maze that brings you back to
ZZ at the outermost level of the maze.

In the first example above, the shortest path is now the loop around
the right side. If the starting level is 0, then taking the
previously-shortest path would pass through BC (to level 1), DE (to
level 2), and FG (back to level 1). Because this is not the outermost
level, ZZ is a wall, and the only option is to go back around to BC,
which would only send you even deeper into the recursive maze.

In the second example above, there is no path that brings you to ZZ at
the outermost level.

Here is a more interesting example:

             Z L X W       C                 
             Z P Q B       K                 
  ###########.#.#.#.#######.###############  
  #...#.......#.#.......#.#.......#.#.#...#  
  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  
  #.#...#.#.#...#.#.#...#...#...#.#.......#  
  #.###.#######.###.###.#.###.###.#.#######  
  #...#.......#.#...#...#.............#...#  
  #.#########.#######.#.#######.#######.###  
  #...#.#    F       R I       Z    #.#.#.#  
  #.###.#    D       E C       H    #.#.#.#  
  #.#...#                           #...#.#  
  #.###.#                           #.###.#  
  #.#....OA                       WB..#.#..ZH
  #.###.#                           #.#.#.#  
CJ......#                           #.....#  
  #######                           #######  
  #.#....CK                         #......IC
  #.###.#                           #.###.#  
  #.....#                           #...#.#  
  ###.###                           #.#.#.#  
XF....#.#                         RF..#.#.#  
  #####.#                           #######  
  #......CJ                       NM..#...#  
  ###.#.#                           #.###.#  
RE....#.#                           #......RF
  ###.###        X   X       L      #.#.#.#  
  #.....#        F   Q       P      #.#.#.#  
  ###.###########.###.#######.#########.###  
  #.....#...#.....#.......#...#.....#.#...#  
  #####.#.###.#######.#######.###.###.#.#.#  
  #.......#.......#.#.#.#.#...#...#...#.#.#  
  #####.###.#####.#.#.#.#.###.###.#.###.###  
  #.......#.....#.#...#...............#...#  
  #############.#.#.###.###################  
               A O F   N                     
               A A D   M                     

One shortest path through the maze is the following:

    Walk from AA to XF (16 steps)
    Recurse into level 1 through XF (1 step)
    Walk from XF to CK (10 steps)
    Recurse into level 2 through CK (1 step)
    Walk from CK to ZH (14 steps)
    Recurse into level 3 through ZH (1 step)
    Walk from ZH to WB (10 steps)
    Recurse into level 4 through WB (1 step)
    Walk from WB to IC (10 steps)
    Recurse into level 5 through IC (1 step)
    Walk from IC to RF (10 steps)
    Recurse into level 6 through RF (1 step)
    Walk from RF to NM (8 steps)
    Recurse into level 7 through NM (1 step)
    Walk from NM to LP (12 steps)
    Recurse into level 8 through LP (1 step)
    Walk from LP to FD (24 steps)
    Recurse into level 9 through FD (1 step)
    Walk from FD to XQ (8 steps)
    Recurse into level 10 through XQ (1 step)
    Walk from XQ to WB (4 steps)
    Return to level 9 through WB (1 step)
    Walk from WB to ZH (10 steps)
    Return to level 8 through ZH (1 step)
    Walk from ZH to CK (14 steps)
    Return to level 7 through CK (1 step)
    Walk from CK to XF (10 steps)
    Return to level 6 through XF (1 step)
    Walk from XF to OA (14 steps)
    Return to level 5 through OA (1 step)
    Walk from OA to CJ (8 steps)
    Return to level 4 through CJ (1 step)
    Walk from CJ to RE (8 steps)
    Return to level 3 through RE (1 step)
    Walk from RE to IC (4 steps)
    Recurse into level 4 through IC (1 step)
    Walk from IC to RF (10 steps)
    Recurse into level 5 through RF (1 step)
    Walk from RF to NM (8 steps)
    Recurse into level 6 through NM (1 step)
    Walk from NM to LP (12 steps)
    Recurse into level 7 through LP (1 step)
    Walk from LP to FD (24 steps)
    Recurse into level 8 through FD (1 step)
    Walk from FD to XQ (8 steps)
    Recurse into level 9 through XQ (1 step)
    Walk from XQ to WB (4 steps)
    Return to level 8 through WB (1 step)
    Walk from WB to ZH (10 steps)
    Return to level 7 through ZH (1 step)
    Walk from ZH to CK (14 steps)
    Return to level 6 through CK (1 step)
    Walk from CK to XF (10 steps)
    Return to level 5 through XF (1 step)
    Walk from XF to OA (14 steps)
    Return to level 4 through OA (1 step)
    Walk from OA to CJ (8 steps)
    Return to level 3 through CJ (1 step)
    Walk from CJ to RE (8 steps)
    Return to level 2 through RE (1 step)
    Walk from RE to XQ (14 steps)
    Return to level 1 through XQ (1 step)
    Walk from XQ to FD (8 steps)
    Return to level 0 through FD (1 step)
    Walk from FD to ZZ (18 steps)

This path takes a total of 396 steps to move from AA at the outermost
layer to ZZ at the outermost layer.

In your maze, when accounting for recursion, how many steps does it
take to get from the open tile marked AA to the open tile marked ZZ,
both at the outermost layer?

Your puzzle answer was 5744.

Both parts of this puzzle are complete! They provide two gold stars: **

'''

import sys
import collections
import math


class Donut(object):
    def __init__(self, map_data):
        self.map_data = map_data
        self.map_h = len(map_data)
        self.map_w = len(map_data[0])
        self.portal_pairs = collections.defaultdict(list)
        self.portal_positions = dict()
        self.outter_wall_positions = set()
        self.inner_wall_positions = set()

    def print_map(self):
        for r in range(0, self.map_h):
            h = "{:2} ".format(r)
            print(h, end='')
            for c in range(0, self.map_w):
                g = self.map_data[r][c]
                print(g, end='')
            print("")
            
        print(" " * len(h), end='')
        for c in range(0, self.map_w):
            print(c % 10, end='')
        print('')
            

    def process_map_data(self):
        for r in range(0, self.map_h):
            for c in range(0, self.map_w):
                g = self.map_data[r][c]
                if g.isalpha() and g.isupper():
                    # find the portal entry
                    (pn, p) = self.find_portal(c, r)
                    if pn and p:
                        print(pn, p)
                        self.portal_pairs[pn].append(p)

        self.portal_pairs_to_positions()
        self.find_outter_inner_walls()
        print("Outter wall:", sorted(self.outter_wall_positions))
        print("Inner wall:", sorted(self.inner_wall_positions))

        
    def find_portal(self, x, y):
        dirs = [(0, -1), # N
                (0, 1), # S
                (-1, 0), # W
                (1, 0)] # E

        p = None
        pn = None
        for d in dirs:
            cx = x + d[0]
            cy = y + d[1]
            if cx >= 0 and cx < self.map_w and \
               cy >= 0 and cy < self.map_h:
                c = self.map_data[cy][cx]
                if c == '.':
                    p = (cx, cy)
                    
                if c.isalpha() and c.isupper():
                    pn = ''.join(sorted(c + self.map_data[y][x]))

                if p and pn:
                    break

        if pn and p:
            return (pn, p)
        else:
            return (None, None)

    def portal_pairs_to_positions(self):
        for k in self.portal_pairs:
            for v in self.portal_pairs[k]:
                self.portal_positions[v] = k

    def portal_another_end(self, s):
        if s in self.portal_positions:
            pn = self.portal_positions[s]
            pair = self.portal_pairs[pn]
            for p in pair:
                if p != s:
                    return p
        else:
            return None

    def find_outter_inner_walls(self):
        d = { "N" : (0, -1),
              "S" : (0, 1),
              "W" : (-1, 0),
              "E" : (1, 0),
              "NE" : (1, -1),
              "SE" : (1, 1),
              "NW" : (-1, -1),
              "SW" : (-1, 1) }
        def dir_pos(x, y, dstr):
            return (x+d[dstr][0], y+d[dstr][1])

        def dir_val(x, y, dstr):

            (nx, ny) = dir_pos(x, y, dstr)
            return self.map_data[ny][nx]

        out_top_left = None
        out_top_right = None
        out_bottom_left = None
        out_bottom_right = None

        in_top_left = None
        in_top_right = None
        in_bottom_left = None
        in_bottom_right = None

        # find all 8 corners of outter and inner walls
        for r in range(1, self.map_h-1):
            for c in range(1, self.map_w-1):
                g = self.map_data[r][c]
                if g == ' ':
                    if dir_val(c, r, "E") == ' ' and \
                       dir_val(c, r, "S") == ' ' and \
                       dir_val(c, r, "SE") == '#':
                        out_top_left = dir_pos(c, r, "SE")
                    elif dir_val(c, r, "W") == ' ' and \
                         dir_val(c, r, "S") == ' ' and \
                         dir_val(c, r, "SW") == '#':
                        out_top_right = dir_pos(c, r, "SW")
                    elif dir_val(c, r, "E") == ' ' and \
                         dir_val(c, r, "N") == ' ' and \
                         dir_val(c, r, "NE") == '#':
                        out_bottom_left = dir_pos(c, r, "NE")
                    elif dir_val(c, r, "W") == ' ' and \
                         dir_val(c, r, "N") == ' ' and \
                         dir_val(c, r, "NW") == '#':
                        out_bottom_right = dir_pos(c, r, "NW")
                elif g == '#':
                    if dir_val(c, r, "E") == '#' and \
                       dir_val(c, r, "S") == '#' and \
                       dir_val(c, r, "SE") == ' ':
                        in_top_left = (c, r)
                    elif dir_val(c, r, "W") == '#' and \
                         dir_val(c, r, "S") == '#' and \
                         dir_val(c, r, "SW") == ' ':
                        in_top_right = (c, r)
                    elif dir_val(c, r, "E") == '#' and \
                         dir_val(c, r, "N") == '#' and \
                         dir_val(c, r, "NE") == ' ':
                        in_bottom_left = (c, r)
                    elif dir_val(c, r, "W") == '#' and \
                         dir_val(c, r, "N") == '#' and \
                         dir_val(c, r, "NW") == ' ':
                        in_bottom_right = (c, r)

        # gen all positions of the wall cells
        for x in range(out_top_left[0], out_top_right[0]+1):
            self.outter_wall_positions.add((x, out_top_left[1]))

        for x in range(out_bottom_left[0], out_bottom_right[0]+1):
            self.outter_wall_positions.add((x, out_bottom_left[1]))

        for y in range(out_top_left[1], out_bottom_left[1]+1):
            self.outter_wall_positions.add((out_top_left[0], y))

        for y in range(out_top_right[1], out_bottom_right[1]+1):
            self.outter_wall_positions.add((out_top_right[0], y))

        for x in range(in_top_left[0], in_top_right[0]+1):
            self.inner_wall_positions.add((x, in_top_left[1]))

        for x in range(in_bottom_left[0], in_bottom_right[0]+1):
            self.inner_wall_positions.add((x, in_bottom_left[1]))

        for y in range(in_top_left[1], in_bottom_left[1]+1):
            self.inner_wall_positions.add((in_top_left[0], y))

        for y in range(in_top_right[1], in_bottom_right[1]+1):
            self.inner_wall_positions.add((in_top_right[0], y))

    def maze_path(self):
        start = self.portal_pairs["AA"][0]
        end = self.portal_pairs["ZZ"][0]
        steps = self.bfs(start, end, set())
        return steps
        
    def bfs(self, s, e, visited):
        q = []
        steps = -1
        q.append(s)
        found_exit = False
        
        while len(q) != 0 and not found_exit:
            qlen = len(q)
            steps += 1
            for i in range(0, qlen):
                n = q.pop(0)
                visited.add(n)
                if n == e:
                    found_exit = True
                    break

                for d in [(0, -1), (0, 1), (-1, 0), (1, 0)]:
                    off_x = n[0] + d[0]
                    off_y = n[1] + d[1]
                    if off_x >= 0 and off_x < self.map_w and \
                       off_y >= 0 and off_y < self.map_h:
                        c = self.map_data[off_y][off_x]
                        if (off_x, off_y) not in visited:
                            if c == ".":
                                q.append((off_x, off_y))

                pn = self.portal_another_end(n)
                if pn and pn not in visited:
                    q.append(pn)

        return steps

    def rec_bfs(self, s, e, level, visited):
        q = []
        steps = -1
        q.append(s)
        found_exit = False

        min_steps = math.inf

        if level < 0:
            return math.inf

        # Here is the trick to stopped the infinite recusive maze
        # For a inner portal, we can go 1 level deeper.
        # If we have only 1 inner portal, we can only go down 1 level.
        # If we are now in level 2 (went down 2 levels), means we visited the
        # same inner portal twice. So if the level is deeper than number of
        # inner portals, we are "too deep" and should not go deeper.
        # A number larger than portal number like 50 as an thresold will not 
        # work since the BFS here assume we found the shortest path for each 
        # level *only* once. If The thresold is large the end goal will be found
        # for multiple times in those "too deep" levels. Hence the result will 
        # be over counted.
        if level > len(self.portal_pairs):
            return math.inf

        while len(q) != 0 and not found_exit:
            qlen = len(q)
            print("level ", level, "q", q)
            for i in range(0, qlen):
                n = q.pop(0)

                print("n", n)

                if n == e:
                    found_exit = True
                    min_steps = steps
                    break

                if (level, n) in visited:
                    continue

                visited.add((level, n))

                for d in [(0, -1), (0, 1), (-1, 0), (1, 0)]:
                    off_x = n[0] + d[0]
                    off_y = n[1] + d[1]
                    if off_x >= 0 and off_x < self.map_w and \
                       off_y >= 0 and off_y < self.map_h:
                        c = self.map_data[off_y][off_x]
                        if (level, (off_x, off_y)) not in visited:
                            if c == ".":
                                q.append((off_x, off_y))

                pn = self.portal_another_end(n)
                if pn:
                    
                    if n in self.outter_wall_positions:
                        l = level - 1
                    elif n in self.inner_wall_positions:
                        l = level + 1

                    # if next level is not 0, close the AA and ZZ
                    if l != 0:
                        p = self.portal_pairs["AA"][0]
                        self.map_data[p[1]][p[0]] = '#'
                        p = self.portal_pairs["ZZ"][0]
                        self.map_data[p[1]][p[0]] = '#'
                    else:
                        p = self.portal_pairs["AA"][0]
                        self.map_data[p[1]][p[0]] = '.'
                        p = self.portal_pairs["ZZ"][0]
                        self.map_data[p[1]][p[0]] = '.'

                    substeps = self.rec_bfs(pn, e, l, visited)

                    # backtrack the AA and ZZ
                    if level != 0:
                        p = self.portal_pairs["AA"][0]
                        self.map_data[p[1]][p[0]] = '#'
                        p = self.portal_pairs["ZZ"][0]
                        self.map_data[p[1]][p[0]] = '#'
                    else:
                        p = self.portal_pairs["AA"][0]
                        self.map_data[p[1]][p[0]] = '.'
                        p = self.portal_pairs["ZZ"][0]
                        self.map_data[p[1]][p[0]] = '.'

                    # visiting portal count as 1 step too
                    if substeps + steps < min_steps:
                        min_steps = substeps + steps + 1
                        found_exit = True

            if found_exit:
                steps = min_steps + 1
            else:
                steps += 1

        if not found_exit:
            return math.inf
        else:
#            print("level ", level, "returning steps", steps)
            return steps

    def maze_recursive_path(self):
        start = self.portal_pairs["AA"][0]
        end = self.portal_pairs["ZZ"][0]
        steps = self.rec_bfs(start, end, 0, set())
        return steps


def main():
    donut_map = []
    line = sys.stdin.readline().strip('\n')
    while line:
        donut_map.append(list(line))
        line = sys.stdin.readline().strip('\n')

    donut = Donut(donut_map)
    donut.print_map()
    
    donut.process_map_data()
    for (k, v) in donut.portal_pairs.items():
        print("{} : {}".format(k, v))
    for (k, v) in donut.portal_positions.items():
        print("{} : {}".format(k, v))

        
    steps = donut.maze_path()
    print("Need steps:", steps)

    steps = donut.maze_recursive_path()
    print("Need Recursive steps:", steps)


if __name__ == "__main__":
    sys.exit(main())
