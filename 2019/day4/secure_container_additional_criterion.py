#!/usr/bin/env python3

'''
--- Part Two ---

An Elf just remembered one more important detail: the two adjacent
matching digits are not part of a larger group of matching digits.

Given this additional criterion, but still ignoring the range rule,
the following are now true:

    112233 meets these criteria because the digits never decrease and
    all repeated digits are exactly two digits long.  
    123444 no longer meets the criteria (the repeated 44 is part of a 
    larger group of 444).  
    111122 meets the criteria (even though 1 is repeated more
    than twice, it still contains a double 22).

How many different passwords within the range given in your puzzle
input meet all of the criteria?


Your puzzle input was 367479-893698.
'''

import sys

def smallest_increase_digits_larger_than(digits: list) -> list:
    nd = [digits[0]]
    chiff = None
    for i in range(1, len(digits)):
        if digits[i] < nd[i-1]:
            nd.append(nd[i-1])
            chiff = nd[i-1]
        else:
            if chiff == None:
                nd.append(digits[i])
            else:
                nd.append(chiff)

    return nd

# This is Monotone Increasing Digits
# https://leetcode.com/articles/monotone-increasing-digits/#
def largest_increase_digits_smaller_than(digits: list) -> list:
    nd = digits[0::]
    cut = 0

    for i in range(1, len(digits)):
        if digits[i] < digits[i-1]:
            cut = i
            break
        
    if cut == 0:
        return nd
        
    for i in range(cut-1, -1, -1):
        nd[i] = digits[i] - 1
        if nd[i] >= nd[i-1]:
            break

    for i in range(cut, len(nd)):
        nd[i] = 9

    return nd

def has_double_digits(x):
    last_d = x % 10
    x = x // 10
    while x > 0:
        d = x % 10
        if d == last_d:
            return True

        x = x // 10
        last_d = d

    return False

def int_to_list(x):
    out = []
    
    while x > 0:
        d = x % 10
        out.append(d)
        x = x // 10

    out.reverse()
    return out

def sub_seq_lens(s:list) -> list:

    curr_c = s[0]
    seq_lens = [1]
    seq_cnt = 0
    for i in range(1, len(s)):
        c = s[i]
        if c == curr_c:
            seq_lens[seq_cnt] += 1
        else:
            seq_lens.append(1)
            seq_cnt += 1
            curr_c = c

    return seq_lens

def has_at_least_one_double_digits(x):
    s = int_to_list(x)

    seq_lens = sub_seq_lens(s)
    if 2 in seq_lens:
        return True
    else:
        return False
        
   

def count_password(start : int, end : int) -> int:
    cnt = 0

    i = start

    while i <= end:
        if has_at_least_one_double_digits(i):
#            print("password:", i)
            cnt += 1

        i += 1
        if i % 10 == 0:
            n = smallest_increase_digits_larger_than(int_to_list(i))
            i = int("".join(map(str, n)))
            

    return cnt

        

def main():
    start = int(sys.stdin.readline().strip())
    end = int(sys.stdin.readline().strip())

    start_digits = int_to_list(start)
    end_digits = int_to_list(end)

    print(start_digits)
    print(end_digits)

    print(smallest_increase_digits_larger_than(start_digits))
    print(largest_increase_digits_smaller_than(end_digits))

    s = int("".join(map(str, smallest_increase_digits_larger_than(start_digits))))
    e = int("".join(map(str, largest_increase_digits_smaller_than(end_digits))))

    print("{} has double: {}".format(s, has_double_digits(s)))

    seq_lens = sub_seq_lens(start_digits)
    print("seq_lens of {} is {}".format(start_digits, seq_lens))

    ans = count_password(s, e)
    print("Password number:", ans)

    return 0

    

if __name__ == "__main__":
    sys.exit(main())
