#!/usr/bin/env python3

'''
--- Day 4: Secure Container ---

You arrive at the Venus fuel depot only to discover it's protected by
a password. The Elves had written the password on a sticky note, but
someone threw it out.

However, they do remember a few key facts about the password:

    It is a six-digit number.  The value is within the range given in
    your puzzle input.  Two adjacent digits are the same (like 22 in
    122345).  Going from left to right, the digits never decrease;
    they only ever increase or stay the same (like 111123 or 135679).

Other than the range rule, the following are true:

    111111 meets these criteria (double 11, never decreases).  223450
    does not meet these criteria (decreasing pair of digits 50).
    123789 does not meet these criteria (no double).

How many different passwords within the range given in your puzzle
input meet these criteria?

Your puzzle input is 367479-893698
'''


import sys

def smallest_increase_digits_larger_than(digits: list) -> list:
    nd = [digits[0]]
    chiff = None
    for i in range(1, len(digits)):
        if digits[i] < nd[i-1]:
            nd.append(nd[i-1])
            chiff = nd[i-1]
        else:
            if chiff == None:
                nd.append(digits[i])
            else:
                nd.append(chiff)

    return nd

# This is Monotone Increasing Digits
# https://leetcode.com/articles/monotone-increasing-digits/#
def largest_increase_digits_smaller_than(digits: list) -> list:
    nd = digits[0::]
    cut = 0

    for i in range(1, len(digits)):
        if digits[i] < digits[i-1]:
            cut = i
            break
        
    if cut == 0:
        return nd
        
    for i in range(cut-1, -1, -1):
        nd[i] = digits[i] - 1
        if nd[i] >= nd[i-1]:
            break

    for i in range(cut, len(nd)):
        nd[i] = 9

    return nd

def has_double_digits(x):
    last_d = x % 10
    x = x // 10
    while x > 0:
        d = x % 10
        if d == last_d:
            return True

        x = x // 10
        last_d = d

    return False

def int_to_list(x):
    out = []
    
    while x > 0:
        d = x % 10
        out.append(d)
        x = x // 10

    out.reverse()
    return out
   

def count_password(start : int, end : int) -> int:
    cnt = 0

    i = start

    while i <= end:
        if has_double_digits(i):
#            print("password:", i)
            cnt += 1

        i += 1
        if i % 10 == 0:
            n = smallest_increase_digits_larger_than(int_to_list(i))
            i = int("".join(map(str, n)))
            

    return cnt

        

def main():
    start = int(sys.stdin.readline().strip())
    end = int(sys.stdin.readline().strip())

    start_digits = int_to_list(start)
    end_digits = int_to_list(end)

    print(start_digits)
    print(end_digits)

    print(smallest_increase_digits_larger_than(start_digits))
    print(largest_increase_digits_smaller_than(end_digits))

    s = int("".join(map(str, smallest_increase_digits_larger_than(start_digits))))
    e = int("".join(map(str, largest_increase_digits_smaller_than(end_digits))))

    print("{} has double: {}".format(s, has_double_digits(s)))

    ans = count_password(s, e)
    print("Password number:", ans)

    return 0

    

if __name__ == "__main__":
    sys.exit(main())
