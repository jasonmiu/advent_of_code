#!/usr/bin/env python3

'''
--- Day 13: Care Package ---

As you ponder the solitude of space and the ever-increasing three-hour
roundtrip for messages between you and Earth, you notice that the
Space Mail Indicator Light is blinking. To help keep you sane, the
Elves have sent you a care package.

It's a new game for the ship's arcade cabinet! Unfortunately, the
arcade is all the way on the other end of the ship. Surely, it won't
be hard to build your own - the care package even comes with
schematics.

The arcade cabinet runs Intcode software like the game the Elves sent
(your puzzle input). It has a primitive screen capable of drawing
square tiles on a grid. The software draws tiles to the screen with
output instructions: every three output instructions specify the x
position (distance from the left), y position (distance from the top),
and tile id. The tile id is interpreted as follows:

    0 is an empty tile. No game object appears in this tile.
    1 is a wall tile. Walls are indestructible barriers.
    2 is a block tile. Blocks can be broken by the ball.
    3 is a horizontal paddle tile. The paddle is indestructible.
    4 is a ball tile. The ball moves diagonally and bounces off objects.

For example, a sequence of output values like 1,2,3,6,5,4 would draw a
horizontal paddle tile (1 tile from the left and 2 tiles from the top)
and a ball tile (6 tiles from the left and 5 tiles from the top).

Start the game. How many block tiles are on the screen when the game exits?

Your puzzle answer was 462.
--- Part Two ---

The game didn't run because you didn't put in any
quarters. Unfortunately, you did not bring any quarters. Memory
address 0 represents the number of quarters that have been inserted;
set it to 2 to play for free.

The arcade cabinet has a joystick that can move left and right. The
software reads the position of the joystick with input instructions:

    If the joystick is in the neutral position, provide 0.
    If the joystick is tilted to the left, provide -1.
    If the joystick is tilted to the right, provide 1.

The arcade cabinet also has a segment display capable of showing a
single number that represents the player's current score. When three
output instructions specify X=-1, Y=0, the third output instruction is
not a tile; the value instead specifies the new score to show in the
segment display. For example, a sequence of output values like
-1,0,12345 would show 12345 as the player's current score.

Beat the game by breaking all the blocks. What is your score after the
last block is broken?

Your puzzle answer was 23981.
'''


import sys
import intcode
import queue

class Game(object):
    def __init__(self, prog, prog_rc):
        self.prog = prog
        self.prog_rc = prog_rc
        self.prog_pc = 0

        self.game_map = None
        self.tiles = None
        self.screen_w = 0
        self.screen_h = 0
        self.score = 0
        self.ball_pos = None
        self.paddle_pos = None

    def game_read_tile(self):
        out_buf = self.prog_rc.out_buf
        x = int(out_buf.get())
        y = int(out_buf.get())
        tile_id = int(out_buf.get())

        return (x, y, tile_id)

    def game_read_init_screen(self):
        out_buf = self.prog_rc.out_buf

        max_x = -99999999
        max_y = -99999999
        game_map = []
        tiles = {}
        while out_buf.qsize() != 0:
            tile = self.game_read_tile()
            (x, y, tid) = tile
            tiles[(x,y)] = tile
        
            if tid == 4:
                self.ball_pos = (x, y)
            elif tid == 3:
                self.paddle_pos = (x, y)
            
            if x > max_x:
                max_x = x
            if y > max_y:
                max_y = y

        self.screen_w = max_x + 1
        self.screen_h = max_y + 1

        self.tiles = tiles
        for i in range(0, self.screen_h):
            game_map.append([0] * self.screen_w)

        for t in tiles:
            (x, y, tid) = tiles[t]
            game_map[y][x] = tid

        self.game_map = game_map

        return game_map

    def count_tile_id(self, tid):
        return [self.tiles[t][2] for t in self.tiles].count(tid)

    def start(self):
        self.prog_pc = intcode.run(self.prog, self.prog_rc, self.prog_pc)
        self.game_read_init_screen()
        return self.prog_pc
    
    def frame(self):
        if intcode.get_opcode_from_pc(self.prog, self.prog_pc) == 99:
            return 0
        self.prog_pc = intcode.run(self.prog, self.prog_rc, self.prog_pc)
        if intcode.get_opcode_from_pc(self.prog, self.prog_pc) == 99:
            return 0
        else:
            return 1

    def play(self):
        self.prog_rc.in_buf.put("0")
        fcnt = 0
        while self.frame() != 0:
            print("---- frame {}, ball ({} {}), paddle ({} {}) score {} ----".\
                  format(fcnt,
                         self.ball_pos[0], self.ball_pos[1],
                         self.paddle_pos[0], self.paddle_pos[1],
                         self.score))
                  
            self.update()
            
            self.print_map()
            self.prog_rc.in_buf.put(self.move_joystick())
            
            fcnt += 1

        self.update() # Read the remaining output
            
        return 0

    def update(self):
        while self.prog_rc.out_buf.qsize() != 0:
            tile = self.game_read_tile()
            (x, y, tid) = tile
            if x != -1:
                self.tiles[(x,y)] = tile
                self.game_map[y][x] = tid
                if tid == 4:
                    self.ball_pos = (x, y)
                elif tid == 3:
                    self.paddle_pos = (x, y)
            elif x == -1 and y == 0:
                self.score = tid
        

    def move_joystick(self):
        if self.paddle_pos[0] == self.ball_pos[0]:
            return "0"
        elif self.paddle_pos[0] < self.ball_pos[0]:
            return "1"
        elif self.paddle_pos[0] > self.ball_pos[0]:
            return "-1"
        

    def print_map(self):
        for i in range(0, self.screen_h):
            for j in range(0, self.screen_w):
                if self.game_map[i][j] == 4:
                    p = '*'
                elif self.game_map[i][j] == 3:
                    p = '='
                elif self.game_map[i][j] == 2:
                    p = '#'
                elif self.game_map[i][j] == 1:
                    p = '|'
                elif self.game_map[i][j] == 0:
                    p = ' '
                print(p, end="")
            print("")



def main():

    if len(sys.argv) != 2:
        print("{} <part [1|2]> [input from stdin]".format(sys.argv[0]))
        return 1
    
    part = sys.argv[1]
    
    line = sys.stdin.readline().strip()
    prog = line.split(',')
    prog_ex = [0] * (len(prog) * 10)
    prog.extend(prog_ex)
    rc = intcode.IntCodeRunConfig()
    rc.use_fd = False
    rc.no_head = True
    rc.in_buf = queue.Queue()
    rc.out_buf = queue.Queue()

    g = Game(prog, rc)

    
    if part == "1":
        g.start()
        g.print_map()
        print("Number of block tiles (id = 2):", g.count_tile_id(2))
        print("PC:", g.prog_pc,
              "OPCODE:", intcode.get_opcode_from_pc(prog, g.prog_pc))
        
        return 0
    
    elif part == "2":
        g.prog[0] = "2"
        g.start()
        g.print_map()

        print("PC:", g.prog_pc,
              "OPCODE:", intcode.get_opcode_from_pc(prog, g.prog_pc))

        g.play()
        print("=== Final Score {} ===".format(g.score))
        
        return 0
        
    

if __name__ == "__main__":
    sys.exit(main())
