#!/usr/bin/env python3

'''
--- Day 18: Many-Worlds Interpretation ---

As you approach Neptune, a planetary security system detects you and
activates a giant tractor beam on Triton! You have no choice but to
land.

A scan of the local area reveals only one interesting feature: a
massive underground vault. You generate a map of the tunnels (your
puzzle input). The tunnels are too narrow to move diagonally.

Only one entrance (marked @) is present among the open passages
(marked .) and stone walls (#), but you also detect an assortment of
keys (shown as lowercase letters) and doors (shown as uppercase
letters). Keys of a given letter open the door of the same letter: a
opens A, b opens B, and so on. You aren't sure which key you need to
disable the tractor beam, so you'll need to collect all of them.

For example, suppose you have the following map:

#########
#b.A.@.a#
#########

Starting from the entrance (@), you can only access a large door (A)
and a key (a). Moving toward the door doesn't help you, but you can
move 2 steps to collect the key, unlocking A in the process:

#########
#b.....@#
#########

Then, you can move 6 steps to collect the only other key, b:

#########
#@......#
#########

So, collecting every key took a total of 8 steps.

Here is a larger example:

########################
#f.D.E.e.C.b.A.@.a.B.c.#
######################.#
#d.....................#
########################

The only reasonable move is to take key a and unlock door A:

########################
#f.D.E.e.C.b.....@.B.c.#
######################.#
#d.....................#
########################

Then, do the same with key b:

########################
#f.D.E.e.C.@.........c.#
######################.#
#d.....................#
########################

...and the same with key c:

########################
#f.D.E.e.............@.#
######################.#
#d.....................#
########################

Now, you have a choice between keys d and e. While key e is closer,
collecting it now would be slower in the long run than collecting key
d first, so that's the best choice:

########################
#f...E.e...............#
######################.#
#@.....................#
########################

Finally, collect key e to unlock door E, then collect key f, taking a
grand total of 86 steps.

Here are a few more examples:

    ########################
    #...............b.C.D.f#
    #.######################
    #.....@.a.B.c.d.A.e.F.g#
    ########################

    Shortest path is 132 steps: b, a, c, d, f, e, g

    #################
    #i.G..c...e..H.p#
    ########.########
    #j.A..b...f..D.o#
    ########@########
    #k.E..a...g..B.n#
    ########.########
    #l.F..d...h..C.m#
    #################

    Shortest paths are 136 steps;
    one is: a, f, b, j, g, n, h, d, l, o, e, p, c, i, k, m

    ########################
    #@..............ac.GI.b#
    ###d#e#f################
    ###A#B#C################
    ###g#h#i################
    ########################

    Shortest paths are 81 steps; one is: a, c, f, i, d, g, b, e, h

How many steps is the shortest path that collects all of the keys?

Your puzzle answer was 5858.
--- Part Two ---

You arrive at the vault only to discover that there is not one vault,
but four - each with its own entrance.

On your map, find the area in the middle that looks like this:

...
.@.
...

Update your map to instead use the correct data:

@#@
###
@#@

This change will split your map into four separate sections, each with its own entrance:

#######       #######
#a.#Cd#       #a.#Cd#
##...##       ##@#@##
##.@.##  -->  #######
##...##       ##@#@##
#cB#Ab#       #cB#Ab#
#######       #######

Because some of the keys are for doors in other vaults, it would take
much too long to collect all of the keys by yourself. Instead, you
deploy four remote-controlled robots. Each starts at one of the
entrances (@).

Your goal is still to collect all of the keys in the fewest steps, but
now, each robot has its own position and can move independently. You
can only remotely control a single robot at a time. Collecting a key
instantly unlocks any corresponding doors, regardless of the vault in
which the key or door is found.

For example, in the map above, the top-left robot first collects key
a, unlocking door A in the bottom-right vault:

#######
#@.#Cd#
##.#@##
#######
##@#@##
#cB#.b#
#######

Then, the bottom-right robot collects key b, unlocking door B in the
bottom-left vault:

#######
#@.#Cd#
##.#@##
#######
##@#.##
#c.#.@#
#######

Then, the bottom-left robot collects key c:

#######
#@.#.d#
##.#@##
#######
##.#.##
#@.#.@#
#######

Finally, the top-right robot collects key d:

#######
#@.#.@#
##.#.##
#######
##.#.##
#@.#.@#
#######

In this example, it only took 8 steps to collect all of the keys.

Sometimes, multiple robots might have keys available, or a robot might
have to wait for multiple keys to be collected:

###############
#d.ABC.#.....a#
######@#@######
###############
######@#@######
#b.....#.....c#
###############

First, the top-right, bottom-left, and bottom-right robots take turns
collecting keys a, b, and c, a total of 6 + 6 + 6 = 18 steps. Then,
the top-left robot can access key d, spending another 6 steps;
collecting all of the keys here takes a minimum of 24 steps.

Here's a more complex example:

#############
#DcBa.#.GhKl#
#.###@#@#I###
#e#d#####j#k#
###C#@#@###J#
#fEbA.#.FgHi#
#############

    Top-left robot collects key a.
    Bottom-left robot collects key b.
    Top-left robot collects key c.
    Bottom-left robot collects key d.
    Top-left robot collects key e.
    Bottom-left robot collects key f.
    Bottom-right robot collects key g.
    Top-right robot collects key h.
    Bottom-right robot collects key i.
    Top-right robot collects key j.
    Bottom-right robot collects key k.
    Top-right robot collects key l.

In the above example, the fewest steps to collect all of the keys is 32.

Here's an example with more choices:

#############
#g#f.D#..h#l#
#F###e#E###.#
#dCba@#@BcIJ#
#############
#nK.L@#@G...#
#M###N#H###.#
#o#m..#i#jk.#
#############

One solution with the fewest steps is:

    Top-left robot collects key e.
    Top-right robot collects key h.
    Bottom-right robot collects key i.
    Top-left robot collects key a.
    Top-left robot collects key b.
    Top-right robot collects key c.
    Top-left robot collects key d.
    Top-left robot collects key f.
    Top-left robot collects key g.
    Bottom-right robot collects key k.
    Bottom-right robot collects key j.
    Top-right robot collects key l.
    Bottom-left robot collects key n.
    Bottom-left robot collects key m.
    Bottom-left robot collects key o.

This example requires at least 72 steps to collect all keys.

After updating your map and using the remote-controlled robots, what
is the fewest steps necessary to collect all of the keys?

Your puzzle answer was 2144.


'''


import sys

class Vault(object):
    def __init__(self):
        self.map = None
        self.map_w = 0
        self.map_h = 0
        self.pos = []
        self.doors = {}
        self.keys = {}
        self.doors_rev = {}
        self.keys_rev = {}
        # a dict of dict
        # { (x1, y1) : { 
        #                 (x2, y2) : (dist, {Door_A, Door_B ...}, {key_a, key_b ...} ),
        #                 (x3, y3) : (dist, {Door_C ...}, {key_i, key_j ...})
        #                  ... ...
        #                 (xn, yn) : ...
        #              },
        #   (x2, y2) : {
        #                 (x1, y1) : ...
        #                 ...
        #                 (xn, yn) :
        #              }
        #   (xn, yn) : { ... }
        # }
        self.feature_dist_matrix = {}
        

    def read_input(self, fd):
        self.map = []
        line = fd.readline().strip()
        i = 0
        while line:
            r = []
            for (j, c) in enumerate(line):
                if c == '@':
                    self.pos.append((j, i))
                    c = '.'

                if c.isalpha() and c.isupper():
                    self.doors[c] = (j, i)
                    self.doors_rev[(j, i)] = c

                if c.isalpha() and not c.isupper():
                    self.keys[c] = (j, i)
                    self.keys_rev[(j, i)] = c
                    
                r.append(c)
            i += 1
            self.map.append(r)
            line = fd.readline().strip()

        self.map_w = len(self.map[0])
        self.map_h = len(self.map)

        return self.map

    def print_map(self):
        i = 0
        for (i, r) in enumerate(self.map):
            print("{:2} ".format(i), end='')
            for (j, c) in enumerate(r):
                if (j, i) in self.pos:
                    c = '*'
                print(c, end="")
            i += 1
            print("")

        print("   ", end='')
        for j in range(0, len(r)):
            print(j % 10, end="")
        print("")

    def shortest_path(self, s_pos, e_pos):
        dirs = { "N":(0, -1),
                 "S":(0, 1),
                 "W":(-1, 0),
                 "E":(1, 0) }

        queue = []
        visited = set()
        queue.append((s_pos, 0, set(), set())) # ((x, y), dist, doors, keys)

        while len(queue) > 0:
            (cur_pos, dist, doors, keys) = queue.pop(0)

            if cur_pos in visited:
                continue
            else:
                visited.add(cur_pos)
            
            g = self.map[cur_pos[1]][cur_pos[0]]
            if g == '#':
                continue

            door = set()
            if g.isalpha() and g.isupper():
                door.add(g)

            key = set()
            if g.isalpha() and not g.isupper():
                key.add(g)

            if cur_pos == e_pos:
                return (dist, doors, keys)

            for d in dirs:
                dxy = dirs[d]
                x = cur_pos[0] + dxy[0]
                y = cur_pos[1] + dxy[1]
                if x >= 0 and x < self.map_w and \
                   y >= 0 and y < self.map_h:
                    queue.append(((x, y), dist + 1, doors|door, keys|key))

        return None

    def all_feature_pos_dists(self):
        for (k, k_pos) in self.keys.items():
            for s_pos in self.pos:
                dist_and_doors_keys = self.shortest_path(s_pos, k_pos)
                if dist_and_doors_keys:
                    (dist, doors, keys) = dist_and_doors_keys
                else:
                    dist = None
                    doors = None
                    keys = None

                if s_pos not in self.feature_dist_matrix:
                    self.feature_dist_matrix[s_pos] = {}

                self.feature_dist_matrix[s_pos][self.keys[k]] = (dist, doors, keys)

        for (k, k_pos) in self.keys.items():
            for (k2, k2_pos) in self.keys.items():
                if k == k2:
                    continue

                dist_and_doors_keys = self.shortest_path(k_pos, k2_pos)
                if dist_and_doors_keys:
                    (dist, doors, keys) = dist_and_doors_keys
                else:
                    dist = None
                    doors = None
                    keys = None
                
                if k_pos not in self.feature_dist_matrix:
                    self.feature_dist_matrix[k_pos] = {}
                
                self.feature_dist_matrix[k_pos][k2_pos] = (dist, doors, keys)
                                

        return self.feature_dist_matrix

    def possible_keys(self, bots_pos, openned_doors):

        # (x,y) : [reachable keys from x,y]
        reachables = {}
        collected_keys = set([d.lower() for d in openned_doors])
        
        for pos in bots_pos:
            r = []
            targets = self.feature_dist_matrix[pos]
        
            for (target, info) in targets.items():
                dist = info[0]
                if dist == None:
                    continue
            
                doors_inbetween = info[1]
                keys_inbetween = info[2]
#                print("doors in between:", doors_inbetween, "opened:", openned_doors)
#                print("keys in between:", keys_inbetween, "collected:", collected_keys)
                if len(doors_inbetween - openned_doors) == 0 and \
                   len(keys_inbetween - collected_keys) == 0:
                    r.append(target)
                

            reachables[pos] = r

        return reachables
                
    
    # collected keys is a set for current currected keys, unordered
    # openned_doors is the set of the current openned_doors
    # memo is a set of caching (pos, collected_keys)
    def get_all_keys(self, bots_pos, path, collected_keys, total_keys_num, memo):

        def key_to_bitmask(key):
            b = 0x01
            k = ord(key) - ord('a')
            return b << k
            
        def keys_set_to_bitmask(keys):
            bits = 0x00
            for k in keys:
                bits = bits | key_to_bitmask(k)
            return bits

        # bots_pos is ((x1,y1),(x2,y2),(x3,y3),(x4,y4))
        memo_idx = (bots_pos, keys_set_to_bitmask(collected_keys))
        if memo_idx in memo:
            return memo[memo_idx]
        
        openned_doors = set(map(str.upper, collected_keys))
        reachables = self.possible_keys(bots_pos, openned_doors)
        
#        print("Reachables from {} : {}".format(bots_pos, reachables))


# Get stuck to a bug here for days.
# The old implementation was accumulate the distances of each feature points,
# from the TOP calls of the recursive function, like:
# def get_all_keys(self, pos, total_dist ...
# ... ...
#          if len(collected_keys) == total_keys_num:
#             return (total_dist, path)
# ... ...
#          (dist, rpath) = self.get_all_keys(k_pos, total_dist + info[0], ...
# ... ...
# While this is a collect algor to find the min distance of all combinations of
# key capturing sequences, the termination condition is saying:
# This is the last key, and total distance of PREVIOUS keys.
# The total distance is affected by the key sequence before this key. If we do a
# DP cache, we want to cache:
# For this position, if we have captured this set of keys, DONT care the order,
# this is the min total distance.
# So if we use the accumulate total distance implementation, we will cache:
# For this position, this is the distance of the key sequence we have captured.
# Then the cached value may not be the min.
# Change the implemenation to accumulate the distance AFTER the recursive
# returns, the termination condition becomes:
# This is the last key, the distance to NEXT key is 0.
# Then the function becomes, given a set of possible next keys with different
# distances, what is the min distance between current pos to a key?
# So the DP will cache, for this position, the MIN distance to the next key,
# hence unrelated to the key capturing sequence before.
# Learnt, the DP has to remember the sub-solutions that are assembling to the
# same direction of the recursive function assembling.

        if len(collected_keys) == total_keys_num:
            return (0, path)

        min_dist = 999999999999
        min_path = None
        for (i, b_pos) in enumerate(bots_pos):
            for k_pos in reachables[b_pos]:
            
                info = self.feature_dist_matrix[b_pos][k_pos]

                k = self.keys_rev[k_pos]
                if k in collected_keys:
                    continue

                new_bot_pos = list(bots_pos)
                new_bot_pos[i] = k_pos
                new_bot_pos = tuple(new_bot_pos)
            
            
                (dist, rpath) = self.get_all_keys(new_bot_pos,
                                                  path + [k],
                                                  collected_keys | set(k),
                                                  total_keys_num, memo)
                dist = info[0] + dist
                if dist < min_dist:
                    min_dist = dist
                    min_path = rpath


                memo_idx = (bots_pos, keys_set_to_bitmask(collected_keys))
                memo[memo_idx] = (min_dist, min_path)
        
        return (min_dist, min_path)
            

    def part2_patch_map(self):
        if len(self.pos) != 1:
            return

        org = self.pos[0]

        patch = ['.', '#', '.',
                 '#', '#', '#',
                 '.', '#', '.']

        offsets = [(-1, -1), (0, -1), (1, -1),
                   (-1, 0), (0, 0), (1, 0),
                   (-1, 1), (0, 1), (1, 1)]

        for (i, off) in enumerate(offsets):
            c = org[0] + off[0]
            r = org[1] + off[1]
            self.map[r][c] = patch[i]

        start_points = ((-1, -1), (1, -1),
                       (-1, 1), (1, 1))

        self.pos = []
        for (i, off) in enumerate(start_points):
            self.pos.append((org[0] + off[0], org[1] + off[1]))


        return self.map


def main():

    if len(sys.argv) != 2 and len(sys.argv) != 3:
        print("{} <part 1|2> [-n] <input from stdin>".format(sys.argv[0]))
        print("  part 1 or 2: 1 or 2 for part 1 or 2")
        print("  -n: if present, input will be intaken as is, will not be patched for part 2")
        return 1
              
    
    vault = Vault()
    vmap = vault.read_input(sys.stdin)
    vault.print_map()
    part = int(sys.argv[1])
    no_patch = False
    if len(sys.argv) == 3:
        no_patch = True if sys.argv[2] == '-n' else False

    if part == 2 and not no_patch:
        vault.part2_patch_map()
        vault.print_map()
    
    
    print("Start Positions:", vault.pos)
    print("Doors:")
    for d in vault.doors:
        print("{}:{}".format(d, vault.doors[d]))

    print("Keys:")
    for k in vault.keys:
        print("{}:{}".format(k, vault.keys[k]))

    m = vault.all_feature_pos_dists()

    for (k, k2) in m.items():
        print(k)
        for e in k2.items():
            print("  ", e)

    (min_dist, min_path) = vault.get_all_keys(tuple(vault.pos), [], set(),
                                              len(vault.keys),
                                              {})
    
    print("Dist: {}, Key sequence: {}".format(min_dist, min_path))
    
    return 0

if __name__ == "__main__":
    sys.exit(main())
