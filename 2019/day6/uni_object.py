class UniObject(object):
    def __init__(self, name, center_of):
        self.name = name
        if center_of != None:
            self.center_of = [center_of]
        else:
            self.center_of = None

def print_map(uo_dict):
    uos = sorted(uo_dict.keys())
    for u in uos:
        uo = uo_dict[u]
        if uo.center_of:
            for c in uo.center_of:
                print("{} -> {}".format(uo.name, c))
        else:
            print("{} -> {}".format(uo.name, None))

def find_uni_boundary(uo_dict):
    uos = list(uo_dict.keys())

    for u in uos:
        uo = uo_dict[u]
        for c in uo.center_of:
            if not c in uo_dict:
                # The orbit object is not a center of any other objects.
                # It is a boundary
                b = UniObject(c, None)
                uo_dict[c] = b

    return uo_dict
            

# The 'total' is the current path depth (number of orbits to COM)
def total_orbits(root, uo_dict, total):
    # Leave node. Return the current path depth
    if root.center_of == None:
        return total
    else:
        # Middle node, aggregate the subtree path depths
        # and the current path depth, then return
        
        sub_total = 0

        for c in root.center_of:
            co = uo_dict[c]
            sub_total += total_orbits(co, uo_dict, (total + 1))
            
        return sub_total + total


def path_to_object(root, dest, path, uo_dict):
#    print("root {} path {}".format(root.name, path))
    if root == None:
        return None

    if root.name == dest.name:
        return path

    if root.center_of:
        for c in root.center_of:
            orbit = uo_dict[c]
            found = path_to_object(orbit, dest, (path + [root]), uo_dict)
            if found:
                return found

    return None

    
