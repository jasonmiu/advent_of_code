#!/usr/bin/env python3
'''
--- Part Two ---

Now, you just need to figure out how many orbital transfers you (YOU)
need to take to get to Santa (SAN).

You start at the object YOU are orbiting; your destination is the
object SAN is orbiting. An orbital transfer lets you move from any
object to an object orbiting or orbited by that object.

For example, suppose you have the following map:

COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN

Visually, the above map of orbits looks like this:

                          YOU
                         /
        G - H       J - K - L
       /           /
COM - B - C - D - E - F
               \
                I - SAN

In this example, YOU are in orbit around K, and SAN is in orbit around
I. To move from K to I, a minimum of 4 orbital transfers are required:

    K to J
    J to E
    E to D
    D to I

Afterward, the map of orbits looks like this:

        G - H       J - K - L
       /           /
COM - B - C - D - E - F
               \
                I - SAN
                 \
                  YOU

What is the minimum number of orbital transfers required to move from
the object YOU are orbiting to the object SAN is orbiting? (Between
the objects they are orbiting - not between YOU and SAN.)
'''


import sys
from uni_object import *

def main():
    uo_dict = {}

    line = sys.stdin.readline().strip()
    # Build the tree.
    # Each node is "current object is center of -> orbit object"
    while line:
        (name, center_of) = line.split(')')

        if not name in uo_dict:
            uo = UniObject(name, center_of)
            uo_dict[name] = uo
        else:
            uo = uo_dict[name]
            uo.center_of.append(center_of)
            
        line = sys.stdin.readline().strip()

    # The out most objects that are not a center of
    # another other object, set their "center_of"
    # to None. They are the leave nodes
    uo_dict = find_uni_boundary(uo_dict)

    com_root = uo_dict["COM"]
    you_node = uo_dict["YOU"]
    san_node = uo_dict["SAN"]

    # Using Lowest Common Ancestor to find the distance between
    # YOU and SAN.

    you_path = path_to_object(com_root, you_node, [], uo_dict)
    for p in you_path:
        print("you path", p.name)

    san_path = path_to_object(com_root, san_node, [], uo_dict)
    for p in san_path:
        print("san path", p.name)

    # We need not to reverse and find from bottom
    # A faster way is start from root, the LCA is the first
    # node that both paths start to diverge, like
    # i = 0 
    # while(i < len(path1) and i < len(path2)): 
    #     if path1[i] != path2[i]: 
    #         break
    #     i += 1
    # return path1[i-1]

    if len(you_path) > len(san_path):
        a = list(reversed(you_path))
        b = list(reversed(san_path))
    else:
        a = list(reversed(san_path))
        b = list(reversed(you_path))

    common_ancestor = None
    a_len_to_ancestor = 0
    b_len_to_ancestor = 0
    
    for i in range(0, len(a)):
        n = a[i]
        for j in range(0, len(b)):
            m = b[j]

            if n.name == m.name:
                common_ancestor = n
                a_len_to_ancestor = i
                b_len_to_ancestor = j
                break
        if common_ancestor:
            break

    print("common_ancestor", common_ancestor.name)
    print("distance", i + j)


if __name__ == "__main__":
    sys.exit(main())
