import sys
import queue

# in bytes
# opcode is 2 bytes
instruction_lens = {
    1: 5,
    2: 5,
    3: 3,
    4: 3,
    5: 4,
    6: 4,
    7: 5,
    8: 5,
    9: 3,
    99: 2
}

# in list elemenet
# opcode + # of operants
instruction_size = {
    1: 4,
    2: 4,
    3: 2,
    4: 2,
    5: 3,
    6: 3,
    7: 4,
    8: 4,
    9: 2,
    99: 1
}

MAX_INS_LEN = 5

def normalize_instruction_len(instruction):

#    print("INS:", instruction, "TYPE", type(instruction))
    
    l = len(instruction)
    d = MAX_INS_LEN - l
    z = ['0'] * d
    i = ''.join(z) + instruction
    return i

def get_opcode(ins):
    oc = int(ins[MAX_INS_LEN-2::])
    return oc

# n is the nth operant, start from 0
def get_operant_mode(instruction, n):
    m = instruction[(MAX_INS_LEN - 3) - n]
    return int(m)

# Mode 2 always means addr = relative_base + operant
def get_value_with_operant_mode(prog, instruction, operant, n, rc):
    m = get_operant_mode(instruction, n)
    addr = 0
    if m == 1:
        return operant
    elif m == 0:
        addr = operant
        return int(prog[addr])
    elif m == 2:
        rb = rc.relative_base
        addr = rb + operant
        return int(prog[addr])

# When a OPCODE can write to an dest, the dest operant
# is the address to be written. Depend on the mode,
# 0 and 1 will both write to the prog[addr], while
# mode 2 means addr = relative_base + operant
def get_addr_with_operant_mode(instruction, operant, n, rc):
    m = get_operant_mode(instruction, n)
    if m == 2:
        addr = rc.relative_base + operant
    else:
        addr = operant

    return addr

    
    
class IntCodeRunConfig(object):
    def __init__(self):
        self.infd = sys.stdin
        self.outfd = sys.stdout
        self.in_buf = None # queue.Queue()
        self.out_buf = None # queue.Queue()
        self.no_head = False
        # True: using blocking file descriptors
        # False: using the nonblocking input/output to queues 
        self.use_fd = True
        self.relative_base = 0
        
def get_opcode_from_pc(prog, pc):
    ins = prog[pc]
    return int(get_opcode(normalize_instruction_len(ins)))

def run(prog, run_config=None, pc=0):

    if not run_config:
        run_config = IntCodeRunConfig()

    print("IN BUF {}, OUT BUF {}".format(run_config.in_buf,
                                         run_config.out_buf))

    pc = pc
    n = len(prog)

    while True:
        print("pc:", pc)
#        print("prog:", prog)
        
        if pc >= (n-1):
            break
        
        ins = prog[pc]
        ins = normalize_instruction_len(ins)
        
        opcode = get_opcode(ins)
        print("instruction {}, opcode {}".format(ins, opcode))

        if opcode == 1 or opcode == 2:
            operant1 = int(prog[pc+1])
            operant2 = int(prog[pc+2])
            dest = int(prog[pc+3])

            v1 = get_value_with_operant_mode(prog, ins, operant1, 0, run_config)
            v2 = get_value_with_operant_mode(prog, ins, operant2, 1, run_config)

            print("Opcode 2, v1", v1, "v2", v2, "dest", dest)

            if opcode == 1:
                ans = v1 + v2
            elif opcode == 2:
                ans = v1 * v2

            addr = get_addr_with_operant_mode(ins, dest, 2, run_config)
            prog[addr] = str(ans)

            pc += instruction_size[opcode]

        elif opcode == 3:
            if not run_config.no_head:
                print("Input: ", end="", flush=True)

            if run_config.use_fd:
                v = run_config.infd.readline().strip()
            else:
                print("In buf len:", run_config.in_buf.qsize())
                if run_config.in_buf.empty():
                    break
                else:
                    v = run_config.in_buf.get()

            if not v.isnumeric():
                raise ValueError("Input value {} is not an integer. opcode {}, pc {}".\
                                 format(v, opcode, pc))
                    
            dest = int(prog[pc+1])
            addr = get_addr_with_operant_mode(ins, dest, 0, run_config)
            prog[addr] = v

            pc += instruction_size[opcode]


        elif opcode == 4:
            operant = int(prog[pc+1])
            v = get_value_with_operant_mode(prog, ins, operant, 0, run_config)

            if not run_config.no_head:
                msg = "Output: {}\n".format(v)
            else:
                msg = "{}".format(v)
                
            if run_config.use_fd:
                fd = run_config.outfd
                fd.write(msg)
            else:
                run_config.out_buf.put(msg)
            
            
            pc += instruction_size[opcode]

        elif opcode == 5 or opcode == 6:
            operant = int(prog[pc+1])
            dest = int(prog[pc+2])

            v1 = get_value_with_operant_mode(prog, ins, operant, 0, run_config)
            v2 = get_value_with_operant_mode(prog, ins, dest, 1, run_config)

            if opcode == 5:
                if v1 != 0:
                    pc = v2
                else:
                    pc += instruction_size[opcode]
            elif opcode == 6:
                if v1 == 0:
                    pc = v2
                else:
                    pc += instruction_size[opcode]

        elif opcode == 7 or opcode == 8:
            operant1 = int(prog[pc+1])
            operant2 = int(prog[pc+2])
            operant3 = int(prog[pc+3])

            v1 = get_value_with_operant_mode(prog, ins, operant1, 0, run_config)
            v2 = get_value_with_operant_mode(prog, ins, operant2, 1, run_config)

            addr = get_addr_with_operant_mode(ins, operant3, 2, run_config)
            
            print("DEST:", addr)

            if opcode == 7:
                if v1 < v2:
                    prog[addr] = "1"
                else:
                    prog[addr] = "0"

            elif opcode == 8:
                if v1 == v2:
                    prog[addr] = "1"
                else:
                    prog[addr] = "0"

            pc += instruction_size[opcode]

        elif opcode == 9:
            operant = int(prog[pc+1])
            v = get_value_with_operant_mode(prog, ins, operant, 0, run_config)

            print("OPCODE 9, upate RB offset", v)
            
            run_config.relative_base += v

            pc += instruction_size[opcode]
                        

        elif opcode == 99:
            break
        else:
            raise ValueError("opcode {}, pc {}".format(opcode, pc))

    return pc


            

        
        

    
