#!/usr/bin/env python3

import sys
import intcode
import queue

# permutation
def all_phase_settings(amp_num, start_from=0):
    settings = []
    amps = list(range(start_from, start_from+amp_num))

    def perm(arr, start):
        n = len(arr)
        if start == n - 1:
            settings.append(arr.copy())
            return
        else:

            
            for i in range(start, n):
                t = arr[start]
                arr[start] = arr[i]
                arr[i] = t
                
                perm(arr, start+1)

                t = arr[start]
                arr[start] = arr[i]
                arr[i] = t

        return

    perm(amps, 0)
    return settings


class Amp(object):
    def __init__(self, amp_id, prog, in_buf=None, out_buf=None):
        self.id = amp_id
        self.prog = prog
        self.in_buf = in_buf
        self.out_buf = out_buf
        self.pc = 0

    def run(self):
        rc = intcode.IntCodeRunConfig()
        rc.no_head = True
        rc.use_fd = False
        rc.in_buf = self.in_buf
        rc.out_buf = self.out_buf
        pc = intcode.run(self.prog, rc, self.pc)
        self.pc = pc
        return pc

    def is_end(self):
        opcode = intcode.get_opcode_from_pc(self.prog, self.pc)
        if opcode == 99:
            return True
        else:
            return False
    

def create_amp_buffers(amp_num):
    amp_buffers = []
    for i in range(0, amp_num):
        amp_buffers.append(queue.Queue())

    return amp_buffers

def assign_amp_buffers(amp, amp_buffers):

    n = len(amp_buffers)
    i = amp.id % n
    j = (amp.id + 1) % n
    amp.in_buf = amp_buffers[i]
    amp.out_buf = amp_buffers[j]

    return amp

def drain_amp_buffer(q):
    while not q.empty():
        q.get()

def drain_all_amp_buffers(amp_buffers):
    for b in amp_buffers:
        drain_amp_buffer(b)


def main():

    prog_file = sys.argv[1]
    fd = open(prog_file, 'r')
    line = fd.readline().strip()
    prog = line.split(',')
    fd.close()
    
    phase_settings = all_phase_settings(5, 5)
    print(phase_settings)
    print("Num of phase settings", len(phase_settings))

    nr_amps = 5

#    phase_setting = [9,8,7,6,5] # p2_test1
#    phase_setting = [9,7,8,5,6] # p2_test2

    amp_buffers = create_amp_buffers(nr_amps)

    max_output = -999999999999999
    max_phase_setting = None

    for phase_setting in phase_settings:
        drain_all_amp_buffers(amp_buffers)
        amps = []

        # Create Amps
        for i in range(0, nr_amps):
            a = Amp(i, prog.copy())
            assign_amp_buffers(a, amp_buffers)
            amps.append(a)

        # Input phase number for each Amp
        for i in range(0, len(amp_buffers)):
            b = amp_buffers[i]
            b.put(str(phase_setting[i]))

        # For first amp, the init input is "0"
        amp_buffers[0].put("0")

        i = 0
        ended = 0
        while True:
            i = i % nr_amps
            a = amps[i]
            pc = a.run()
            print("--- amp {} stopped".format(a.id))
            if a.is_end():
                ended += 1

            if ended == nr_amps:
                break

            i += 1

        output = int(amp_buffers[0].get())
        print("Output:", output)
        if output > max_output:
            max_output = output
            max_phase_setting = phase_setting

    print("--- Done ---")
    print("Max output: {}, Max phase setting: {}".\
          format(max_output, max_phase_setting))
        
    return 0
        

if __name__ == "__main__":
    sys.exit(main())
