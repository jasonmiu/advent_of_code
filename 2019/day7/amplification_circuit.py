#!/usr/bin/env python3

import sys
from intcode import *
import os

# permutation
def all_phase_settings(amp_num):
    settings = []
    amps = list(range(0, amp_num))

    def perm(arr, start):
        n = len(arr)
        if start == n - 1:
            settings.append(arr.copy())
            return
        else:

            
            for i in range(start, n):
                t = arr[start]
                arr[start] = arr[i]
                arr[i] = t
                
                perm(arr, start+1)

                t = arr[start]
                arr[start] = arr[i]
                arr[i] = t

        return

    perm(amps, 0)
    return settings


def run_amp(prog, phase, sig):

    infile = "input_" + str(phase)
    outfile = "output_" + str(phase)
    
    infd = open(infile, 'w+')
    infd.write("{}\n{}\n".format(phase, sig))
    infd.close()
    infd = open(infile, 'r')
    outfd = open(outfile, 'w+')
    rc = IntCodeRunConfig()
    rc.infd = infd
    rc.outfd = outfd
    rc.no_head = True

    run(prog, rc)

    infd.close()
    outfd.close()

    return (infile, outfile)

def read_amp_output(phase):
    outfile = "output_" + str(phase)
    outfd = open(outfile, 'r')
    sig = outfd.read().strip()

    return sig

def main():

    prog_file = sys.argv[1]
    fd = open(prog_file, 'r')
    line = fd.readline().strip()
    prog = line.split(',')
    fd.close()
    
    phase_settings = all_phase_settings(5)
    print(phase_settings)
    print("Num of phase settings", len(phase_settings))

    ### --- Testing block start ---
    (f1, f2) = run_amp(prog.copy(), 4, 0)
    with open(f2, 'r') as fd:
        print("Phase {}, output {}".format(4, fd.readline().strip()))
    sig = read_amp_output(4)
    print("Phase {} read back sig {}".format(4, sig))

    os.remove(f1)
    os.remove(f2)
    ### --- Testing block end ---

    max_sig = 0
    max_phase_setting = None
    for phase_setting in phase_settings:
        temp_files = []
        sig = 0
        for p in phase_setting:
            (infile, outfile) = run_amp(prog.copy(), p, sig)
            temp_files.extend((infile, outfile))
            sig = int(read_amp_output(p))
            if sig > max_sig:
                max_sig = sig
                max_phase_setting = phase_setting

        print(temp_files)
        for f in temp_files:
            os.remove(f)

    print("Max Output Sig: {}, Max Phase setting: {}".\
          format(max_sig, max_phase_setting))
        


    
        

if __name__ == "__main__":
    sys.exit(main())
