class SpaceImage(object):
    def __init__(self, row, col, data):
        self.row = row
        self.col = col
        self.layers = []
        self.layer_zero_counts = None

        for l in range(0, len(data) // (row * col)):
            image = []
            for i in range(0, self.row):
                r = [0] * self.col
                image.append(r)

            self.layers.append(image)

        self.layer_zero_counts = [0] * len(self.layers)


        for i in range(0, len(data)):
            # mod self.row for rotating the layers
            r = (i // self.col) % self.row
            c = i % self.col
            # Think rows are in a continuous scroll, fold each self.rows
            # for a layer
            l = (i // self.col) // self.row
#            print("i {}, r {}, c {}, l {}".format(i, r, c, l))
            
            self.layers[l][r][c] = data[i]
            if data[i] == 0:
                self.layer_zero_counts[l] += 1

    def print_layers(self):
        for l in range(0, len(self.layers)):
            print("Layer:", l)
            for r in range(0, self.row):
                for c in range(0, self.col):
                    print(self.layers[l][r][c], end="")

                print("")

    def layer_zero_count(self, layer_idx):
        image = self.layers[layer_idx]
        cnt = 0
        for r in range(0, self.row):
            for c in range(0, self.col):
                if image[r][c] == 0:
                    cnt += 1

        return cnt

    def get_visible_color(self, r, c, start_layer):
        nr_layer = len(self.layers)
        p = self.layers[start_layer][r][c]
        
        if start_layer == (nr_layer - 1):
            return p

        if p == 0 or p == 1:
            return p
        elif p == 2:
            return self.get_visible_color(r, c, start_layer+1)

    def get_visible_image(self):

        result = []

        for r in range(0, self.row):
            result.append([0] * self.col)
            
            for c in range(0, self.col):
                result[r][c] = self.get_visible_color(r, c, 0)

        return result

                
            
