def run(prog):
    pc = 0
    n = len(prog)
    while True:
        if pc >= n:
            break
        
        opcode = prog[pc]
    
        if opcode == 1 or opcode == 2:
            operant1 = prog[pc+1]
            operant2 = prog[pc+2]
            dest = prog[pc+3]

            v1 = prog[operant1]
            v2 = prog[operant2]
            
            if opcode == 1:
                ans = v1 + v2
            elif opcode == 2:
                ans = v1 * v2

            prog[dest] = ans
            pc += 4
            
        elif opcode == 99:
            break
        else:
            raise ValueError("opcode {}, pc {}".format(opcode, pc))

    return pc
