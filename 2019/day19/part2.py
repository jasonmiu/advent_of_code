#!/usr/bin/env python3
'''
--- Part Two ---

You aren't sure how large Santa's ship is. You aren't even sure if
you'll need to use this thing on Santa's ship, but it doesn't hurt to
be prepared. You figure Santa's ship might fit in a 100x100 square.

The beam gets wider as it travels away from the emitter; you'll need
to be a minimum distance away to fit a square of that size into the
beam fully. (Don't rotate the square; it should be aligned to the same
axes as the drone grid.)

For example, suppose you have the following tractor beam readings:

#.......................................
.#......................................
..##....................................
...###..................................
....###.................................
.....####...............................
......#####.............................
......######............................
.......#######..........................
........########........................
.........#########......................
..........#########.....................
...........##########...................
...........############.................
............############................
.............#############..............
..............##############............
...............###############..........
................###############.........
................#################.......
.................########OOOOOOOOOO.....
..................#######OOOOOOOOOO#....
...................######OOOOOOOOOO###..
....................#####OOOOOOOOOO#####
.....................####OOOOOOOOOO#####
.....................####OOOOOOOOOO#####
......................###OOOOOOOOOO#####
.......................##OOOOOOOOOO#####
........................#OOOOOOOOOO#####
.........................OOOOOOOOOO#####
..........................##############
..........................##############
...........................#############
............................############
.............................###########

In this example, the 10x10 square closest to the emitter that fits
entirely within the tractor beam has been marked O. Within it, the
point closest to the emitter (the only highlighted O) is at X=25,
Y=20.

Find the 100x100 square closest to the emitter that fits entirely
within the tractor beam; within that square, find the point closest to
the emitter. What value do you get if you take that point's X
coordinate, multiply it by 10000, then add the point's Y coordinate?
(In the example above, this would be 250020.)

Your puzzle answer was 6671097.
'''


import sys
import intcode
import queue

'''
+--------------------- x
|\`
| \ `
|  \  `
|   \   `
|    \    ` 
|     \     `
|      \      `  upper line
|       \ shadow`  
|  lower \   area `
|  line   \         ` 
|          \          `
|           \           `
|<-guess x -><-- >= d  ->
y  segment

'''


def find_upper_line(grid):
    for x in range(len(grid[0]) - 1, -1 , -1):
        for y in range(0, len(grid)):
            if grid[y][x] == 1:
                return (x, y)
    return None


def find_lower_line(grid):
    for y in range(len(grid) - 1, -1, -1):
        for x in range(0, len(grid[0])):
            if grid[y][x] == 1:
                return (x, y)
    return None

def line_func(x, y):
    return lambda i: y/x * i


# Print a part of the grid.
# x1,y1 is the right most position of middle line
# Will show upper and lower 2 lines
def display_region(x1, y1, x2, y2, d, prog):

    prog_rc = intcode.IntCodeRunConfig()
    prog_rc.use_fd = False
    prog_rc.no_head = True
    prog_rc.in_buf = queue.Queue()
    prog_rc.out_buf = queue.Queue()

    out = []
    for j in range(0, 5):
        out.append([0] * d)

    c = 0
    for x in range(x1 - d + 1, x1 + 1):
        r = 0
        for y in range(y1-2, y1+3):
            p = prog.copy()
            pc = intcode.run(p, prog_rc, 0)
            prog_rc.in_buf.put(str(x))
            pc = intcode.run(p, prog_rc, pc)
            prog_rc.in_buf.put(str(y))
            pc = intcode.run(p, prog_rc, pc)
            output = prog_rc.out_buf.get()
            out[r][c] = output

            r += 1
        c += 1

    for i in range(0, 5):
        for j in range(0, d):
            print(out[i][j], end='')
        print('')


def check_point(x, y, prog):

    prog_rc = intcode.IntCodeRunConfig()
    prog_rc.use_fd = False
    prog_rc.no_head = True
    prog_rc.in_buf = queue.Queue()
    prog_rc.out_buf = queue.Queue()

    p = prog.copy()
    pc = intcode.run(p, prog_rc, 0)
    prog_rc.in_buf.put(str(x))
    pc = intcode.run(p, prog_rc, pc)
    prog_rc.in_buf.put(str(y))
    pc = intcode.run(p, prog_rc, pc)
    output = int(prog_rc.out_buf.get())

    return output

def guess_start_x(grid, start_y, d):
    import math

    (x, y) = find_upper_line(grid)
    # alpha is the angle between y-axis to the upper line
    # 1.578 is the radian of 90deg, as the angle by default is from x-axis
    alpha = 1.578 -  math.atan(y/x) 

    return math.tan(alpha) * start_y - d


# brute force scan from left to right, top to down
# using upper left corner x0,y0 as the pivot,
# and test the upper right x1,y1, and lower left x2,y2
# to see if all 3 points are in the shadow area. If so,
# the frist one is the closest one.
def scan(x, y, d, prog):
    x0 = x
    y0 = y

    while True:
        ul_met_shadow = False
        # scan along x-axis. assume the upper left will pass thru shadow area
        while True:

            out = check_point(x0, y0, prog)
#            print(x0, y0, out, ul_met_shadow)

            if out == 0 and ul_met_shadow:
                break

            if not ul_met_shadow:
                if out == 1:
                    ul_met_shadow = True

                x0 += 1
                continue

            x1 = x0 + d - 1
            y1 = y0
            x2 = x0
            y2 = y0 + d - 1
            if out == 1:
                o1 = check_point(x1, y1, prog)
                o2 = check_point(x2, y2, prog)

                if o1 == 1 and o2 == 1:
                    return (x0, y0)

            x0 += 1

        x0 = x
        y0 += 1
        print("y0", y0)

    return None


def main():
    grid = []

    if len(sys.argv) != 2:
        print("Read the map produced from part 1 and give ans of part 2")
        print("USAGE: {} <intcode prog> <stdin: map file output from part 1>".format(sys.argv[0]))
        return 1
              

    with open(sys.argv[1], 'r') as fd:
        line = fd.readline().strip()
        prog = line.split(',')
    prog_ex = [0] * (len(prog) * 3000)
    prog.extend(prog_ex)

    line = sys.stdin.readline().strip()
    while line:
        grid.append(list(map(int, line)))
        line = sys.stdin.readline().strip()

    d = 100
    y = 700
    # some optimization here. We want to scan the square as close
    # the shadow area as possible. Since the width of shadow area must
    # >= d, the width of the square, and from the graph we can see the shadow width
    # w ~= y / 7, so we can start scanning from this y.
    # The left handside of the graph contains a lot of zeros and not interesting,
    # So we try to start the x close to the lower line
    x = guess_start_x(grid, y, d)
    (x0, y0) = scan(int(x), y, d, prog)

    print("ans:", x0 * 10000 + y0)



    return 0

if __name__ == "__main__":
    sys.exit(main())
