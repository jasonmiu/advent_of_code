#!/usr/bin/env python3
'''
--- Day 19: Tractor Beam ---

Unsure of the state of Santa's ship, you borrowed the tractor beam
technology from Triton. Time to test it out.

When you're safely away from anything else, you activate the tractor
beam, but nothing happens. It's hard to tell whether it's working if
there's nothing to use it on. Fortunately, your ship's drone system
can be configured to deploy a drone to specific coordinates and then
check whether it's being pulled. There's even an Intcode program (your
puzzle input) that gives you access to the drone system.

The program uses two input instructions to request the X and Y
position to which the drone should be deployed. Negative numbers are
invalid and will confuse the drone; all numbers should be zero or
positive.

Then, the program will output whether the drone is stationary (0) or
being pulled by something (1). For example, the coordinate X=0, Y=0 is
directly in front of the tractor beam emitter, so the drone control
program will always report 1 at that location.

To better understand the tractor beam, it is important to get a good
picture of the beam itself. For example, suppose you scan the 10x10
grid of points closest to the emitter:

       X
  0->      9
 0#.........
 |.#........
 v..##......
  ...###....
  ....###...
Y .....####.
  ......####
  ......####
  .......###
 9........##

In this example, the number of points affected by the tractor beam in
the 10x10 area closest to the emitter is 27.

However, you'll need to scan a larger area to understand the shape of
the beam. How many points are affected by the tractor beam in the
50x50 area closest to the emitter? (For each of X and Y, this will be
0 through 49.)

Your puzzle answer was 173.

'''


import sys
import intcode
import queue

class Tractor(object):
    def __init__(self, prog):
        self.prog = prog
        self.prog_rc = intcode.IntCodeRunConfig()
        self.prog_rc.use_fd = False
        self.prog_rc.no_head = True
        self.prog_rc.in_buf = queue.Queue()
        self.prog_rc.out_buf = queue.Queue()

        self.area_w = 50
        self.area_h = 50
        self.area = []
        for i in range(0, self.area_h):
            self.area.append([0] * self.area_w)

    def scan_area(self):
        for i in range(0, self.area_h):
            for j in range(0, self.area_w):
                p = self.prog.copy()
                pc = intcode.run(p, self.prog_rc, 0)
                self.prog_rc.in_buf.put(str(j))
                pc = intcode.run(p, self.prog_rc, pc)
                self.prog_rc.in_buf.put(str(i))
                pc = intcode.run(p, self.prog_rc, pc)
                output = self.prog_rc.out_buf.get()
                self.area[i][j] = int(output)




    def print_area(self):
        for i in range(0, self.area_h):
            print("{:2} ".format(i), end='')
            for j in range(0, self.area_w):
                print(self.area[i][j], end='')
            print("")
        print("   ", end='')
        for j in range(0, self.area_w):
            print("{}".format(j % 10), end="")
        print("")

    def save_area(self, fd):
        for i in range(0, self.area_h):
            for j in range(0, self.area_w):
                print(self.area[i][j], end='', file=fd)
            print("", file=fd)

        

    def points_affected(self):
        total = 0
        for i in range(0, self.area_h):
            total = total + self.area[i].count(1)

        return total
                
        

def main():

    if len(sys.argv) != 3:
        print("Print the map of part 1, also output map file for part 2")
        print("USAGE: {} <intcode prog> <map output file>".format(sys.argv[0]))
        return 1
    
    with open(sys.argv[1], 'r') as fd:
        line = fd.readline().strip()
        prog = line.split(',')

#    seems like need to reserve at least 50*50
    prog_ex = [0] * (len(prog) * 3000) 
    prog.extend(prog_ex)

#    intcode.run(prog)

    tractor = Tractor(prog)
    tractor.scan_area()
    tractor.print_area()

    print("Total points affected:", tractor.points_affected())

    with open(sys.argv[2], 'w') as fd:
        tractor.save_area(fd)

if __name__ == "__main__":
    sys.exit(main())
