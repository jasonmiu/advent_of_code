#!/usr/bin/env python3
'''
--- Day 11: Space Police ---

On the way to Jupiter, you're pulled over by the Space Police.

"Attention, unmarked spacecraft! You are in violation of Space Law!
All spacecraft must have a clearly visible registration identifier!
You have 24 hours to comply or be sent to Space Jail!"

Not wanting to be sent to Space Jail, you radio back to the Elves on
Earth for help. Although it takes almost three hours for their reply
signal to reach you, they send instructions for how to power up the
emergency hull painting robot and even provide a small Intcode program
(your puzzle input) that will cause it to paint your ship
appropriately.

There's just one problem: you don't have an emergency hull painting
robot.

You'll need to build a new emergency hull painting robot. The robot
needs to be able to move around on the grid of square panels on the
side of your ship, detect the color of its current panel, and paint
its current panel black or white. (All of the panels are currently
black.)

The Intcode program will serve as the brain of the robot. The program
uses input instructions to access the robot's camera: provide 0 if the
robot is over a black panel or 1 if the robot is over a white
panel. Then, the program will output two values:

    First, it will output a value indicating the color to paint the
    panel the robot is over: 0 means to paint the panel black, and 1
    means to paint the panel white.  Second, it will output a value
    indicating the direction the robot should turn: 0 means it should
    turn left 90 degrees, and 1 means it should turn right 90 degrees.

After the robot turns, it should always move forward exactly one
panel. The robot starts facing up.

The robot will continue running for a while like this and halt when it
is finished drawing. Do not restart the Intcode computer inside the
robot during this process.

For example, suppose the robot is about to start running. Drawing
black panels as ., white panels as #, and the robot pointing the
direction it is facing (< ^ > v), the initial state and region near
the robot looks like this:

.....
.....
..^..
.....
.....

The panel under the robot (not visible here because a ^ is shown
instead) is also black, and so any input instructions at this point
should be provided 0. Suppose the robot eventually outputs 1 (paint
white) and then 0 (turn left). After taking these actions and moving
forward one panel, the region now looks like this:

.....
.....
.<#..
.....
.....

Input instructions should still be provided 0. Next, the robot might
output 0 (paint black) and then 0 (turn left):

.....
.....
..#..
.v...
.....

After more outputs (1,0, 1,0):

.....
.....
..^..
.##..
.....

The robot is now back where it started, but because it is now on a
white panel, input instructions should be provided 1. After several
more outputs (0,1, 1,0, 1,0), the area looks like this:

.....
..<#.
...#.
.##..
.....

Before you deploy the robot, you should probably have an estimate of
the area it will cover: specifically, you need to know the number of
panels it paints at least once, regardless of color. In the example
above, the robot painted 6 panels at least once. (It painted its
starting panel twice, but that panel is still only counted once; it
also never painted the panel it ended on.)

Build a new emergency hull painting robot and run the Intcode program
on it. How many panels does it paint at least once?

Your puzzle answer was 1564.  --- Part Two ---

You're not sure what it's trying to paint, but it's definitely not a
registration identifier. The Space Police are getting impatient.

Checking your external ship cameras again, you notice a white panel
marked "emergency hull painting robot starting panel". The rest of the
panels are still black, but it looks like the robot was expecting to
start on a white panel, not a black one.

Based on the Space Law Space Brochure that the Space Police attached
to one of your windows, a valid registration identifier is always
eight capital letters. After starting the robot on a single white
panel instead, what registration identifier does it paint on your
hull?

Your puzzle answer was RFEPCFEB.
'''


import sys
import intcode
import queue

class PaintingRobot(object):
    UP = 0
    RIGHT = 1
    DOWN = 2
    LEFT = 3
    BLACK = 0
    WHITE = 1

    DIR_MAP = { 0:'^',
                1:'>',
                2:'v',
                3:'<'}
    
    def __init__(self, map_w, map_h, prog):
        self.map_w = map_w
        self.map_h = map_h
        self.map = []
        self.direction = PaintingRobot.UP
        self.pos = (map_w // 2, map_h // 2)
        self.prog = prog
        rc = intcode.IntCodeRunConfig()
        rc.no_head = True
        rc.use_fd = False
        rc.in_buf = queue.Queue()
        rc.out_buf = queue.Queue()
        self.prog_rc = rc
        self.prog_pc = 0
        
        self.visited = set() # set of (x, y)

        for h in range(0, map_h):
            self.map.append([0] * map_w)

    def get_current_panel(self):
        return self.map[self.pos[1]][self.pos[0]]

    def paint_current_panel(self, color):
        self.map[self.pos[1]][self.pos[0]] = color
        return self.pos

    def prog_input(self):
        i = str(self.get_current_panel())
        self.prog_rc.in_buf.put(i)
        print("Robot input:", i)
        return

    def prog_output(self):
        output = [0] * 2
        output[0] = int(self.prog_rc.out_buf.get())
        output[1] = int(self.prog_rc.out_buf.get())
        print("Robot output:", output)
        return output

    def print_map(self):
        for h in range(0, self.map_h):
            for w in range(0, self.map_w):
                p = '.' if self.map[h][w] == 0 else '#'
                if (w, h) == self.pos:
                    p = PaintingRobot.DIR_MAP[self.direction]
                print(p, end='')
            print("")

    def move_step(self):

        self.prog_input()
        pc = intcode.run(self.prog, self.prog_rc, self.prog_pc)
        self.prog_pc = pc
        opcode = intcode.get_opcode_from_pc(self.prog, pc)
        prog_ended = False
        if opcode != 99:
            output = self.prog_output()
            self.paint_current_panel(output[0])
            self.visited.add(self.pos)

            r = output[1]

            if r == 0:
                d = (self.direction - 1) % 4 # Turn Left
            elif r == 1:
                d = (self.direction + 1) % 4 # Turn Right
            
            if d == PaintingRobot.UP:
                self.direction = PaintingRobot.UP
                self.pos = (self.pos[0], self.pos[1] - 1) # upper left is (0,0)
            elif d == PaintingRobot.DOWN:
                self.direction = PaintingRobot.DOWN
                self.pos = (self.pos[0], self.pos[1] + 1)
            elif d == PaintingRobot.RIGHT:
                self.direction = PaintingRobot.RIGHT
                self.pos = (self.pos[0] + 1, self.pos[1])
            elif d == PaintingRobot.LEFT:
                self.direction = PaintingRobot.LEFT
                self.pos = (self.pos[0] - 1, self.pos[1])

            if self.pos[0] < 0 or self.pos[1] < 0:
                raise ValueError("Robot position out of map")

        else:
            prog_ended = True

        return prog_ended

    def paint(self):
        stop = False
        while not stop:
            stop = self.move_step()
        


def main():
    prog = None

    if len(sys.argv) != 5:
        print("{} <prog file> <map width> <map hight> <question part [1|2]>".format(sys.argv[0]))
        return 1
              

    with open(sys.argv[1], 'r') as fd:
        line = fd.readline().strip()
        prog = line.split(',')

    prog_ex = [0] * (10 * len(prog))
    prog.extend(prog_ex) # extra space for prog
    map_w = int(sys.argv[2])
    map_h = int(sys.argv[3])
    question_part = int(sys.argv[4])
    if question_part == 1:
        bot = PaintingRobot(map_w, map_h, prog)
        bot.print_map()
        print("---")

        bot.paint()
        bot.print_map()

        print("Paint at least once:", len(bot.visited))
    elif question_part == 2:
        bot = PaintingRobot(map_w, map_h, prog)
        bot.paint_current_panel(PaintingRobot.WHITE)
        bot.paint()
        bot.print_map()
    else:
        print("question part must be 1 or 2")
    

if __name__ == "__main__":
    sys.exit(main())
