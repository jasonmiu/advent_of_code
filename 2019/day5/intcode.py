import sys

# in bytes
# opcode is 2 bytes
instruction_lens = {
    1: 5,
    2: 5,
    3: 3,
    4: 3,
    5: 4,
    6: 4,
    7: 5,
    8: 5,
    99: 2
}

# in list elemenet
# opcode + # of operants
instruction_size = {
    1: 4,
    2: 4,
    3: 2,
    4: 2,
    5: 3,
    6: 3,
    7: 4,
    8: 4,
    99: 1
}

MAX_INS_LEN = 5

def normalize_instruction_len(instruction):

#    print("INS:", instruction, "TYPE", type(instruction))
    
    l = len(instruction)
    d = MAX_INS_LEN - l
    z = ['0'] * d
    i = ''.join(z) + instruction
    return i

def get_opcode(ins):
    oc = int(ins[MAX_INS_LEN-2::])
    return oc

# n is the nth operant, start from 0
def get_operant_mode(instruction, n):
    m = instruction[MAX_INS_LEN - (3+n)]
    return int(m)
    
    

def run(prog):
    pc = 0
    n = len(prog)
    print("prog len", n)

    while True:
        print("pc:", pc)
#        print("prog:", prog)
        
        if pc >= (n-1):
            break
        
        ins = prog[pc]
        ins = normalize_instruction_len(ins)
        
        opcode = get_opcode(ins)
        print("instruction {}, opcode {}".format(ins, opcode))

        if opcode == 1 or opcode == 2:
            operant1 = int(prog[pc+1])
            operant2 = int(prog[pc+2])
            dest = int(prog[pc+3])

            m = get_operant_mode(ins, 0)
            v1 = prog[operant1] if m == 0 else operant1
            v1 = int(v1)

            m = get_operant_mode(ins, 1)
            v2 = prog[operant2] if m == 0 else operant2
            v2 = int(v2)
        
            if opcode == 1:
                ans = v1 + v2
            elif opcode == 2:
                ans = v1 * v2

            # Parameters that an instruction writes to will
            # never be in immediate mode.
            prog[dest] = str(ans)

            pc += instruction_size[opcode]

        elif opcode == 3:
            print("Input:")
            v = sys.stdin.readline().strip()
            if not v.isnumeric():
                raise ValueError("Input value {} is not an integer. opcode {}, pc {}".\
                                 format(v, opcode, pc))
                    
            dest = int(prog[pc+1])
            prog[dest] = v
            pc += instruction_size[opcode]


        elif opcode == 4:
            m = get_operant_mode(ins, 0)
            operant = prog[pc+1]
            if m == 0:
                v = prog[int(operant)]
            else:
                v = operant

            print("Output: {}".format(v))
            pc += instruction_size[opcode]

        elif opcode == 5 or opcode == 6:
            operant = int(prog[pc+1])
            dest = int(prog[pc+2])

            m = get_operant_mode(ins, 0)
            v1 = int(prog[operant]) if m == 0 else operant

            m = get_operant_mode(ins, 1)
            v2 = int(prog[dest]) if m == 0 else dest

            if opcode == 5:
                if v1 != 0:
                    pc = v2
                else:
                    pc += instruction_size[opcode]
            elif opcode == 6:
                if v1 == 0:
                    pc = v2
                else:
                    pc += instruction_size[opcode]

        elif opcode == 7 or opcode == 8:
            operant1 = int(prog[pc+1])
            operant2 = int(prog[pc+2])
            dest = int(prog[pc+3])

            m = get_operant_mode(ins, 0)
            v1 = int(prog[operant1]) if m == 0 else operant1
                
            m = get_operant_mode(ins, 1)
            v2 = int(prog[operant2]) if m == 0 else operant2

            print("DEST:", dest)

            if opcode == 7:
                if v1 < v2:
                    prog[dest] = "1"
                else:
                    prog[dest] = "0"

            elif opcode == 8:
                if v1 == v2:
                    prog[dest] = "1"
                else:
                    prog[dest] = "0"

            pc += instruction_size[opcode]
            

        elif opcode == 99:
            break
        else:
            raise ValueError("opcode {}, pc {}".format(opcode, pc))

    return pc


            

        
        

    
