#!/usr/bin/env python3

'''
--- Part Two ---

All this drifting around in space makes you wonder about the nature of
the universe. Does history really repeat itself? You're curious
whether the moons will ever return to a previous state.

Determine the number of steps that must occur before all of the moons'
positions and velocities exactly match a previous point in time.

For example, the first example above takes 2772 steps before they
exactly match a previous point in time; it eventually returns to the
initial state:

After 0 steps:
pos=<x= -1, y=  0, z=  2>, vel=<x=  0, y=  0, z=  0>
pos=<x=  2, y=-10, z= -7>, vel=<x=  0, y=  0, z=  0>
pos=<x=  4, y= -8, z=  8>, vel=<x=  0, y=  0, z=  0>
pos=<x=  3, y=  5, z= -1>, vel=<x=  0, y=  0, z=  0>

After 2770 steps:
pos=<x=  2, y= -1, z=  1>, vel=<x= -3, y=  2, z=  2>
pos=<x=  3, y= -7, z= -4>, vel=<x=  2, y= -5, z= -6>
pos=<x=  1, y= -7, z=  5>, vel=<x=  0, y= -3, z=  6>
pos=<x=  2, y=  2, z=  0>, vel=<x=  1, y=  6, z= -2>

After 2771 steps:
pos=<x= -1, y=  0, z=  2>, vel=<x= -3, y=  1, z=  1>
pos=<x=  2, y=-10, z= -7>, vel=<x= -1, y= -3, z= -3>
pos=<x=  4, y= -8, z=  8>, vel=<x=  3, y= -1, z=  3>
pos=<x=  3, y=  5, z= -1>, vel=<x=  1, y=  3, z= -1>

After 2772 steps:
pos=<x= -1, y=  0, z=  2>, vel=<x=  0, y=  0, z=  0>
pos=<x=  2, y=-10, z= -7>, vel=<x=  0, y=  0, z=  0>
pos=<x=  4, y= -8, z=  8>, vel=<x=  0, y=  0, z=  0>
pos=<x=  3, y=  5, z= -1>, vel=<x=  0, y=  0, z=  0>

Of course, the universe might last for a very long time before
repeating. Here's a copy of the second example from above:

<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>

This set of initial positions takes 4686774924 steps before it repeats
a previous state! Clearly, you might need to find a more efficient way
to simulate the universe.

How many steps does it take to reach the first state that exactly
matches a previous state?

Your puzzle answer was 318382803780324.

------
This part is actually asking, 
How many steps does it take to reach the first state that 
exactly matches * the initial state*?
Open question:
Is it possible that there is a state repeated in the middle of the 
period of the initial state repeating? ie.


0-1-2-3-4-5-6-7-8-9-0---> time  
1 0 3 2 4 5 8 4 6 9 1  <- states 
        ^-----^        <- a state repeated a previous state
^-------------------^  <- initial state repeating period

From https://www.reddit.com/r/adventofcode/comments/e9vfod/2019_day_12_i_see_everyone_solving_it_with_an/

it's actually guaranteed that the cycle returns to the initial state
first!

One way of explaining this is to think of each step like a math
function that takes in the current state (positions and velocities)
and outputs the next state. What's important to notice is that this
function is invertible, meaning we can calculate it in reverse. We can
take a state, subtract the velocities one step, then do the opposite
of the gravity calculation, and get the unique previous state!

Since the previous state is unique, the only way to get to any state
in the middle of the cycle is to follow the sequence of states we
already took to get there. Otherwise, the entry point to our cycle
would have multiple previous states. The only way for the state to
loop back around without creating a fork like that is for it to link
back up to the beginning.

'''


import sys
from moon import *
from gcd_lcm import *

def step_time(moons, dim):
    n = len(moons)
    for i in range(0, n):
        m1 = moons[i]
        for j in range(i+1, n):
            m2 = moons[j]
            # For each time step, each dimension can be updated independently
            # from other dimensions for the position and velocity vectors.
            # So instead of computing the whole vectors, compute only
            # 1 dimension.
            (v1, v2) = m1.apply_gravity(m2, dim)

    dv = []
    for i in range(0, n):
        moons[i].apply_velocity()
        dv.append([moons[i].pos[dim], moons[i].velocity[dim]])

    return dv


def dimension_repeat(moons, dim):
    init_dv = []
    for i in range(0, len(moons)):
        init_dv.append([moons[i].pos[dim], moons[i].velocity[dim]])

    i = 1
    while True:
        # Check if this dimension equal the same dimension of the initial state.
        # We want to know how many time steps it takes to repeat
        # the initial state for * one dimension *.
        # The first time step that repeat the initial state will be the
        # LCM of all dimension, as it is the first (least common multiple) time
        # steps all dimension repeat the inital state.
        dv = step_time(moons, dim)
        if dv == init_dv:
            break
        i += 1

    return i
    

def main():
    moons = []

    line = sys.stdin.readline().strip()
    print("Initial state")
    while line:
        line = line.strip("<>")
        eles = line.split(',')
        v = [int(x.split('=')[1]) for x in eles]
        m = Moon(Vector(v))
        moons.append(m)
        print(m)
        line = sys.stdin.readline().strip()

    n1 = dimension_repeat(moons, 0)
    print("Dim 0 repeated after {} steps".format(n1))
    n2 = dimension_repeat(moons, 1)
    print("Dim 1 repeated after {} steps".format(n2))
    n3 = dimension_repeat(moons, 2)
    print("Dim 2 repeated after {} steps".format(n3))

    print("After {} steps, the sysmte returns to the init state".\
          format(lcm([n1, n2, n3])))
                                                                    
    
    return 0

if __name__ == "__main__":
    sys.exit(main())
