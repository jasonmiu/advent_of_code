class Vector(object):
    def __init__(self, v) -> None:
        self.v = v
        self.len = len(v)

    def __getitem__(self, key):
        return self.v[key]

    def __setitem__(self, key, val):
        self.v[key] = val

    def __str__(self):
        return str(self.v)

    def __add__(self, u):
        return Vector(list(map(lambda x: sum(x), zip(self, u))))

    def __eq__(self, other):
        if isinstance(other, Vector):
            return self.v == other.v
        
def test_vector():

    u = Vector([1, 2, 3])
    v = Vector([10, 20, 30])
    w = Vector([10, 20, 30])

    print("u:", u, "v:", v, "w:", w)
    print("u[0] {}, u[1] {}, u[-1] {}".format(u[0], u[1], u[-1]))
    print("u + v:", u + v)
    print("u == v:", u == v, "v == w:", v == w)

    w[0] = 10
    w[1] = w[1] + 80
    w[2] += 70
    print("w:", w)
    print("---")
    

class Moon(object):
    def __init__(self, pos) -> None:
        self.pos = pos
        self.velocity = Vector([0,0,0])

    def __str__(self):
        return "{} -> {}".format(self.pos, self.velocity)

    def apply_gravity(self, other, dimension=None) -> tuple:

        if dimension == None:
            start_d = 0
            end_d = self.pos.len
        else:
            start_d = dimension
            end_d = dimension+1

        for i in range(start_d, end_d):
            if self.pos[i] > other.pos[i]:
                self.velocity[i] = self.velocity[i] - 1
                other.velocity[i] = other.velocity[i] + 1
            elif self.pos[i] < other.pos[i]:
                self.velocity[i] = self.velocity[i] + 1
                other.velocity[i] = other.velocity[i] - 1
                
        return (self.velocity, other.velocity)

    def apply_velocity(self):
        new_pos = self.pos + self.velocity
        self.pos = new_pos
        return new_pos

    def kinetic_energy(self):
        return sum(map(abs, self.velocity))

    def potential_energy(self):
        return sum(map(abs, self.pos))

    def energy(self):
        return self.potential_energy() * self.kinetic_energy()
