import math

def _gcd(a, b):
    if a == 0:
        return b
    if b == 0:
        return a
    return _gcd(b, (a % b))

def gcd(nums:list) -> int:
    if len(nums) == 2:
        return _gcd(nums[0], nums[1])
    else:
        g = _gcd(nums[0], nums[1])
        return gcd([g] + nums[2::])

def _lcm(a, b):
    return (a * b) // _gcd(a, b)

def lcm(nums:list) -> int:
    if len(nums) == 2:
        return _lcm(nums[0], nums[1])
    else:
        m = _lcm(nums[0], nums[1])
        return lcm([m] + nums[2::])


def main():
    print("GCD(20, 35):", _gcd(20, 35))
    print("MATH.GCD(20, 35):", math.gcd(20, 35))

    print("GCD(19, 7):", _gcd(19, 7))
    print("MATH.GCD(19, 7):", math.gcd(19, 7))

    print("GCD(18,28,22):", gcd([18,28,22]))
    print("GCD(1024,408,76, 36):", gcd([1024, 408, 76, 36]))

    print("LCM(18, 22):", _lcm(18, 22))
    print("LCM(198, 28):", _lcm(198, 28))
    print("LCM(18, 22, 28):", lcm([18, 22, 28]))

if __name__ == "__main__":
    main()
