#!/usr/bin/env python3

'''
--- Day 16: Flawed Frequency Transmission ---

You're 3/4ths of the way through the gas giants. Not only do roundtrip
signals to Earth take five hours, but the signal quality is quite bad
as well. You can clean up the signal with the Flawed Frequency
Transmission algorithm, or FFT.

As input, FFT takes a list of numbers. In the signal you received
(your puzzle input), each number is a single digit: data like 15243
represents the sequence 1, 5, 2, 4, 3.

FFT operates in repeated phases. In each phase, a new list is
constructed with the same length as the input list. This new list is
also used as the input for the next phase.

Each element in the new list is built by multiplying every value in
the input list by a value in a repeating pattern and then adding up
the results. So, if the input list were 9, 8, 7, 6, 5 and the pattern
for a given element were 1, 2, 3, the result would be 9*1 + 8*2 + 7*3
+ 6*1 + 5*2 (with each input element on the left and each value in the
repeating pattern on the right of each multiplication). Then, only the
ones digit is kept: 38 becomes 8, -17 becomes 7, and so on.

While each element in the output array uses all of the same input
array elements, the actual repeating pattern to use depends on which
output element is being calculated. The base pattern is 0, 1, 0,
-1. Then, repeat each value in the pattern a number of times equal to
the position in the output list being considered. Repeat once for the
first element, twice for the second element, three times for the third
element, and so on. So, if the third element of the output list is
being calculated, repeating the values would produce: 0, 0, 0, 1, 1,
1, 0, 0, 0, -1, -1, -1.

When applying the pattern, skip the very first value exactly once. (In
other words, offset the whole pattern left by one.) So, for the second
element of the output list, the actual pattern used would be: 0, 1, 1,
0, 0, -1, -1, 0, 0, 1, 1, 0, 0, -1, -1, ....

After using this process to calculate each element of the output list,
the phase is complete, and the output list of this phase is used as
the new input list for the next phase, if any.

Given the input signal 12345678, below are four phases of FFT. Within
each phase, each output digit is calculated on a single line with the
result at the far right; each multiplication operation shows the input
digit on the left and the pattern value on the right:

Input signal: 12345678

1*1  + 2*0  + 3*-1 + 4*0  + 5*1  + 6*0  + 7*-1 + 8*0  = 4
1*0  + 2*1  + 3*1  + 4*0  + 5*0  + 6*-1 + 7*-1 + 8*0  = 8
1*0  + 2*0  + 3*1  + 4*1  + 5*1  + 6*0  + 7*0  + 8*0  = 2
1*0  + 2*0  + 3*0  + 4*1  + 5*1  + 6*1  + 7*1  + 8*0  = 2
1*0  + 2*0  + 3*0  + 4*0  + 5*1  + 6*1  + 7*1  + 8*1  = 6
1*0  + 2*0  + 3*0  + 4*0  + 5*0  + 6*1  + 7*1  + 8*1  = 1
1*0  + 2*0  + 3*0  + 4*0  + 5*0  + 6*0  + 7*1  + 8*1  = 5
1*0  + 2*0  + 3*0  + 4*0  + 5*0  + 6*0  + 7*0  + 8*1  = 8

After 1 phase: 48226158

4*1  + 8*0  + 2*-1 + 2*0  + 6*1  + 1*0  + 5*-1 + 8*0  = 3
4*0  + 8*1  + 2*1  + 2*0  + 6*0  + 1*-1 + 5*-1 + 8*0  = 4
4*0  + 8*0  + 2*1  + 2*1  + 6*1  + 1*0  + 5*0  + 8*0  = 0
4*0  + 8*0  + 2*0  + 2*1  + 6*1  + 1*1  + 5*1  + 8*0  = 4
4*0  + 8*0  + 2*0  + 2*0  + 6*1  + 1*1  + 5*1  + 8*1  = 0
4*0  + 8*0  + 2*0  + 2*0  + 6*0  + 1*1  + 5*1  + 8*1  = 4
4*0  + 8*0  + 2*0  + 2*0  + 6*0  + 1*0  + 5*1  + 8*1  = 3
4*0  + 8*0  + 2*0  + 2*0  + 6*0  + 1*0  + 5*0  + 8*1  = 8

After 2 phases: 34040438

3*1  + 4*0  + 0*-1 + 4*0  + 0*1  + 4*0  + 3*-1 + 8*0  = 0
3*0  + 4*1  + 0*1  + 4*0  + 0*0  + 4*-1 + 3*-1 + 8*0  = 3
3*0  + 4*0  + 0*1  + 4*1  + 0*1  + 4*0  + 3*0  + 8*0  = 4
3*0  + 4*0  + 0*0  + 4*1  + 0*1  + 4*1  + 3*1  + 8*0  = 1
3*0  + 4*0  + 0*0  + 4*0  + 0*1  + 4*1  + 3*1  + 8*1  = 5
3*0  + 4*0  + 0*0  + 4*0  + 0*0  + 4*1  + 3*1  + 8*1  = 5
3*0  + 4*0  + 0*0  + 4*0  + 0*0  + 4*0  + 3*1  + 8*1  = 1
3*0  + 4*0  + 0*0  + 4*0  + 0*0  + 4*0  + 3*0  + 8*1  = 8

After 3 phases: 03415518

0*1  + 3*0  + 4*-1 + 1*0  + 5*1  + 5*0  + 1*-1 + 8*0  = 0
0*0  + 3*1  + 4*1  + 1*0  + 5*0  + 5*-1 + 1*-1 + 8*0  = 1
0*0  + 3*0  + 4*1  + 1*1  + 5*1  + 5*0  + 1*0  + 8*0  = 0
0*0  + 3*0  + 4*0  + 1*1  + 5*1  + 5*1  + 1*1  + 8*0  = 2
0*0  + 3*0  + 4*0  + 1*0  + 5*1  + 5*1  + 1*1  + 8*1  = 9
0*0  + 3*0  + 4*0  + 1*0  + 5*0  + 5*1  + 1*1  + 8*1  = 4
0*0  + 3*0  + 4*0  + 1*0  + 5*0  + 5*0  + 1*1  + 8*1  = 9
0*0  + 3*0  + 4*0  + 1*0  + 5*0  + 5*0  + 1*0  + 8*1  = 8

After 4 phases: 01029498

Here are the first eight digits of the final output list after 100
phases for some larger inputs:

    80871224585914546619083218645595 becomes 24176176.
    19617804207202209144916044189917 becomes 73745418.
    69317163492948606335995924319873 becomes 52432133.

After 100 phases of FFT, what are the first eight digits in the final
output list?

Your puzzle answer was 34841690.
--- Part Two ---

Now that your FFT is working, you can decode the real signal.

The real signal is your puzzle input repeated 10000 times. Treat this
new signal as a single input list. Patterns are still calculated as
before, and 100 phases of FFT are still applied.

The first seven digits of your initial input signal also represent the
message offset. The message offset is the location of the eight-digit
message in the final output list. Specifically, the message offset
indicates the number of digits to skip before reading the eight-digit
message. For example, if the first seven digits of your initial input
signal were 1234567, the eight-digit message would be the eight digits
after skipping 1,234,567 digits of the final output list. Or, if the
message offset were 7 and your final output list were
98765432109876543210, the eight-digit message would be 21098765. (Of
course, your real message offset will be a seven-digit number, not a
one-digit number like 7.)

Here is the eight-digit message in the final output list after 100
phases. The message offset given in each input has been
highlighted. (Note that the inputs given below are repeated 10000
times to find the actual starting input lists.)

    03036732577212944063491565474664 becomes 84462026.
    02935109699940807407585447034323 becomes 78725270.
    03081770884921959731165446850517 becomes 53553731.

After repeating your input signal 10000 times and running 100 phases
of FFT, what is the eight-digit message embedded in the final output
list?

Your puzzle answer was 48776785.
'''

'''
I am using the trick that the offset is larger than the half way of the 
output array. What if the offset is smaller than the midpoint then the 
output need to be computed with the 1st half of the coefficient pattern?
Can we do something like:
https://www.reddit.com/r/adventofcode/comments/ebxz7f/2019_day_16_part_2_visualization_and_hardmode/
'''

import sys


def iter_repeating_pattern(repeat):
    base = [0, 1, 0, -1]
    n = len(base)

    i = 0
    while True:
        for r in range(0, repeat):
            yield base[i % n]
        
        i += 1

def test_iter():
    p = iter_repeating_pattern(1)
    for i in range(0, 8):
        print(next(p))

    print("---")
    p = iter_repeating_pattern(2)        
    for i in range(0, 8):
        print(next(p))

    print("---")
    p = iter_repeating_pattern(3)        
    for i in range(0, 8):
        print(next(p))

def output_digit(num_str, digit):
    p = iter_repeating_pattern(digit + 1)
    next(p) # discard first pattern digit
    s = 0
    for i in num_str:
        x = int(i)
        s = s + (x * next(p))

    return (abs(s) % 10) # right most digit

def phase(num_str):
    n = len(num_str)

    out = []
    for i in range(0, n):
        s = output_digit(num_str, i)
        out.append(str(s))

    return "".join(out)

def print_output_digit(num_str, digit):
    p = iter_repeating_pattern(digit + 1)
    next(p) # discard first pattern digit
    s = 0
    for i in num_str:
        x = int(i)
        q = next(p)
        print("{:2d} ".format(q), end="")
        s = s + (x * q)

    print(" = {}".format(s))

        
def print_phase(num_str):
    n = len(num_str)

    for i in range(0, n):
        print("{:2d} ".format(int(num_str[i])), end="")
    print("")
    
    out = []
    for i in range(0, n):
        print_output_digit(num_str, i)

    for i in range(0, n):
        print("{:2d} ".format(i), end="")
    print("")

    
def sum_digit_from(nums, start, last_sum):
    # optimization here. For i + 1 digit, it is
    # the sum from nums[i + 1] to nums[n]. While for i digit,
    # it the the sum from
    # nums[i] + nums[(i+1)] + nums[(i+2)] + ... + nums[n]
    # So the sum from nums[i+1] to nums[n] is sum from i - num[i+1]
    if last_sum != None:
        s = last_sum - nums[start-1]
    else:
        s = sum(nums[start::])

    return (s % 10, s)

def phase2(nums, offset):
    n = len(nums)
    out = [0] * n
    last_sum = None
    
    for i in range(offset, n):
        (s, summed) = sum_digit_from(nums, i, last_sum)
        out[i] = s
        last_sum = summed

    return out


def main():
#    test_iter()

    in_num = sys.stdin.readline().strip()

    print(output_digit(in_num, 1))

    print(phase(in_num))

    sig = in_num
    for i in range(0, 100):
        out = phase(sig)
        print("rount ", i, "output:", out)
        sig = out

    # Show the coefficient pattern
    print_phase(in_num)

    in_num_int = list(map(int, in_num))

    real_in_num = in_num_int * 10000

    offset = int(in_num[0:7])

    print("sum_digit_from(in_num, 16)", sum_digit_from(in_num_int, 16, None))

    sig = real_in_num
    for i in range(0, 100):
        print("Phase", i)
        # This is assuming the offset is starting over the half point of the
        # result array. Since the 2nd half of the coefficient from the repeated
        # patterns are all 1s till the end of the input array, so we only
        # need to sum up the input[i] to input[n] for digit i.
        sig = phase2(sig, offset) 

    ans = "".join(list(map(str, sig[offset:offset+8])))
    print("Offset {} of output: {}".format(offset, ans))

    

if __name__ == "__main__":
    sys.exit(main())
