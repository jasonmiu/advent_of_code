#!/usr/bin/env python3

'''
--- Day 10: Monitoring Station ---

You fly into the asteroid belt and reach the Ceres monitoring
station. The Elves here have an emergency: they're having trouble
tracking all of the asteroids and can't be sure they're safe.

The Elves would like to build a new monitoring station in a nearby
area of space; they hand you a map of all of the asteroids in that
region (your puzzle input).

The map indicates whether each position is empty (.) or contains an
asteroid (#). The asteroids are much smaller than they appear on the
map, and every asteroid is exactly in the center of its marked
position. The asteroids can be described with X,Y coordinates where X
is the distance from the left edge and Y is the distance from the top
edge (so the top-left corner is 0,0 and the position immediately to
its right is 1,0).

Your job is to figure out which asteroid would be the best place to
build a new monitoring station. A monitoring station can detect any
asteroid to which it has direct line of sight - that is, there cannot
be another asteroid exactly between them. This line of sight can be at
any angle, not just lines aligned to the grid or diagonally. The best
location is the asteroid that can detect the largest number of other
asteroids.

For example, consider the following map:

.#..#
.....
#####
....#
...##

The best location for a new monitoring station on this map is the
highlighted asteroid at 3,4 because it can detect 8 asteroids, more
than any other location. (The only asteroid it cannot detect is the
one at 1,0; its view of this asteroid is blocked by the asteroid at
2,2.) All other asteroids are worse locations; they can detect 7 or
fewer other asteroids. Here is the number of other asteroids a
monitoring station on each asteroid could detect:

.7..7
.....
67775
....7
...87

Here is an asteroid (#) and some examples of the ways its line of
sight might be blocked. If there were another asteroid at the location
of a capital letter, the locations marked with the corresponding
lowercase letter would be blocked and could not be detected:

#.........
...A......
...B..a...
.EDCG....a
..F.c.b...
.....c....
..efd.c.gb
.......c..
....f...c.
...e..d..c

Here are some larger examples:

    Best is 5,8 with 33 other asteroids detected:

    ......#.#.
    #..#.#....
    ..#######.
    .#.#.###..
    .#..#.....
    ..#....#.#
    #..#....#.
    .##.#..###
    ##...#..#.
    .#....####

    Best is 1,2 with 35 other asteroids detected:

    #.#...#.#.
    .###....#.
    .#....#...
    ##.#.#.#.#
    ....#.#.#.
    .##..###.#
    ..#...##..
    ..##....##
    ......#...
    .####.###.

    Best is 6,3 with 41 other asteroids detected:

    .#..#..###
    ####.###.#
    ....###.#.
    ..###.##.#
    ##.##.#.#.
    ....###..#
    ..#.#..#.#
    #..#.#.###
    .##...##.#
    .....#.#..

    Best is 11,13 with 210 other asteroids detected:

    .#..##.###...#######
    ##.############..##.
    .#.######.########.#
    .###.#######.####.#.
    #####.##.#.##.###.##
    ..#####..#.#########
    ####################
    #.####....###.#.#.##
    ##.#################
    #####.##.###..####..
    ..######..##.#######
    ####.##.####...##..#
    .#####..#.######.###
    ##...#.##########...
    #.##########.#######
    .####.#.###.###.#.##
    ....##.##.###..#####
    .#.#.###########.###
    #.#.#.#####.####.###
    ###.##.####.##.#..##

Find the best location for a new monitoring station. How many other
asteroids can be detected from that location?

'''

import sys
import math
import collections
from point_segment import *

            
def test_line_segment():
    points = [
        Point(0,0), #0
        Point(0,1), #1
        Point(0,2), #2
        Point(1,1), #3
        Point(0,10), #4
        Point(1,2), #5
        Point(1, -3) #6
    ]
    
    s1 = LineSegment(points[0], points[2])
    
    cp_s1 = s1.cross_product(points[1])
    print("Line", s1, "cross product at point", points[1], ":", cp_s1)
    print("has point {} : {}".format(points[1], s1.has_point(points[1])))

    cp_s1 = s1.cross_product(points[3])
    print("Line", s1, "cross product at point", points[3], ":", cp_s1)
    print("has point {} : {}".format(points[3], s1.has_point(points[3])))

    cp_s1 = s1.cross_product(points[4])
    print("Line", s1, "cross product at point", points[4], ":", cp_s1)
    print("has point {} : {}".format(points[4], s1.has_point(points[4])))

    s2 = LineSegment(points[3], points[5])
    cp_s2 = s2.cross_product(points[6])
    print("Line", s2, "cross product at point", points[6], ":", cp_s2)
    print("has point {} : {}".format(points[6], s1.has_point(points[6])))

    cp_s2 = s2.cross_product(points[4])
    print("Line", s2, "cross product at point", points[4], ":", cp_s2)
    print("has point {} : {}".format(points[4], s1.has_point(points[4])))

def main():

    points = []
    points_rev_idx = {}
    
    line = sys.stdin.readline().strip()
    y = 0
    i = 0
    while line:
        x = 0
        for p in line:
            if p == '#':
                point = Point(x, y)
                points.append(point)
                points_rev_idx[point] = i
                i += 1
            x += 1

        y += 1
        line = sys.stdin.readline().strip()

#    print(*points, sep="\n")

    # Indices are matching the indices of 'points'
    # Boolean to indicate if a point can see other points
    # Default all points can see each others, except self.
    # Since we will only check points with the same slope,
    # other points with different slopes are visiable be default
    # and will not be updated during the checking core
    sight_map = []
    for i in range(0, len(points)):
        r = [True] * len(points)
        r[i] = False # set self cannot see self
        sight_map.append(r)


    # Two points A, B, can for a line segment, ie. see each other,
    # if no 3rd point in the middle of this segment.
    # The Brute Force algor will be for each pair of points,
    # check all points if there is a 3rd point lay on the segment
    # To speed up, we do not need to check all points, just need to
    # check the points that have the same slope of the current
    # checking segment, since for a point C that can lay on a segment,
    # the slope of AC and BC must equal to slope of AB.
    #
    # This is building such data structure
    # slopes_at_point [A] [slope 1] -> [B, C, D ...]
    #                     [slope 2] -> [E, ...]
    #                     [slope 3] -> [G]
    #                     ... ...
    #                 [B] [slope 1] -> [A, C, F ...]
    #                     [slope 2] -> [H, ...]
    #                     ... ...
    #                 [C] [slope 1] -> [A, B]
    #                 ...
    slopes_at_point = collections.defaultdict()
    for a in points:
        for b in points:
            if a != b:
                seg = LineSegment(a, b)

                if a in slopes_at_point:
                    points_follow_slopes = slopes_at_point[a]
                else:
                    points_follow_slopes = collections.defaultdict(list)
                    slopes_at_point[a] = points_follow_slopes
                    
                points_follow_slopes[seg.slope].append(b)
                
    print("Slope at point:", points[0])
    for s in slopes_at_point[points[0]]:
        p = slopes_at_point[points[0]][s]
        print("slope:", s, "points:", *p)
                

#    print("Slopes dict:", slopes_dict)
#    test_line_segment()

    # Algorithm check core
    for a in points:
        a_idx = points_rev_idx[a]
        
        for b in points:
            b_idx = points_rev_idx[b]

            # The sight_map is symmetric, we update both ends
            # of a segment if both ends cannot see each other.
            # If we know they cannot see each others already,
            # skip checking
            if sight_map[a_idx][b_idx] == False:
                continue
            
            if a != b:
                seg = LineSegment(a, b)
                points_with_same_slopes = slopes_at_point[a][seg.slope]
                for p in points_with_same_slopes:
                    if p != b:
                        if seg.has_point(p):
                            sight_map[a_idx][b_idx] = False
                            sight_map[b_idx][a_idx] = False


    max_visable = -9999999999
    max_visable_position = -1
    for i in range(0, len(points)):
        visable = len(list(filter(lambda x : x == True, sight_map[i])))
#        print(sight_map[i], "see:", visable)
        if visable > max_visable:
            max_visable = visable
            max_visable_position = i

    print("---")
    print("Max visiable: {}, position: {}".format(max_visable,
                                                  points[max_visable_position]))

    

if __name__ == "__main__":
    sys.exit(main())
