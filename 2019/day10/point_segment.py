import math

class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def __str__(self):
        return ("({},{})".format(self.x, self.y))

    def __eq__(self, p):
        if self.x == p.x and self.y == p.y:
            return True
        else:
            return False

    def __hash__(self):
        return hash(str(self))

    def get_degree(self):
        h = math.sqrt(pow(self.x, 2) + pow(self.y, 2))
        d = math.degrees(math.asin(self.y / h))

        x = self.x
        y = self.y

        e = 0.0001
        
        if abs(90.0 - d) < e and y >= 0:
            return 0.0

        if abs(90.0 - d) < e and y < 0:
            return 180.0

        if abs(0.0 - d) < e and x >= 0:
            return 90.0

        if abs(0.0 - d) < e and x < 0:
            return 270.0

        if d >= 0:
            if x >= 0:
                return 90.0 - d
            else:
                return 270 + d
        else:
            if x >= 0:
                return 90.0 + abs(d)
            else:
                return 270 - abs(d)
        
        return d

    def get_translate(self, origin):
        # The original coodination system has +ve y pointing downward
        x = self.x - origin.x
        y = origin.y - self.y
        return Point(x, y)

class LineSegment(object):
    def __init__(self, a:Point, b:Point) -> None:
        self.a = a
        self.b = b
        self.len = math.sqrt(pow((a.x - b.x), 2) + pow((a.y - b.y), 2))
        self.slope = self.slope(self.a, self.b)

    def __str__(self):
        return ("{}->{}".format(self.a, self.b))

    def __eq__(self, s):
        if self.a == s.a and self.b == s.b:
            return True
        else:
            return False
        

    def slope(self, a, b):
        v = (a.y - b.y)
        u = (a.x - b.x)
        if u != 0:
            return v / u
        else:
            return None

    def cross_product(self, c:Point) -> float:
        ax = c.x - self.a.x
        ay = c.y - self.a.y
        bx = c.x - self.b.x
        by = c.y - self.b.y

        return (ax * by) - (bx * ay)
        
    def has_point(self, c):
        if self.cross_product(c) != 0:
            return False

        if self.a.x >= self.b.x:
            x1 = self.a.x
            x2 = self.b.x
        else:
            x1 = self.b.x
            x2 = self.a.x

        if self.a.y >= self.b.y:
            y1 = self.a.y
            y2 = self.b.y
        else:
            y1 = self.b.y
            y2 = self.a.y

        if (x1 >= c.x >=x2) and (y1 >= c.y >= y2):
            return True
        else:
            return False

    def has_end_point(self, c):
        if c == self.a or c == self.b:
            return True
        else:
            return False
