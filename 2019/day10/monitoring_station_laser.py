#!/usr/bin/env python3

'''
--- Part Two ---

Once you give them the coordinates, the Elves quickly deploy an
Instant Monitoring Station to the location and discover the worst:
there are simply too many asteroids.

The only solution is complete vaporization by giant laser.

Fortunately, in addition to an asteroid scanner, the new monitoring
station also comes equipped with a giant rotating laser perfect for
vaporizing asteroids. The laser starts by pointing up and always
rotates clockwise, vaporizing any asteroid it hits.

If multiple asteroids are exactly in line with the station, the laser
only has enough power to vaporize one of them before continuing its
rotation. In other words, the same asteroids that can be detected can
be vaporized, but if vaporizing one asteroid makes another one
detectable, the newly-detected asteroid won't be vaporized until the
laser has returned to the same position by rotating a full 360
degrees.

For example, consider the following map, where the asteroid with the
new monitoring station (and laser) is marked X:

.#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....X...###..
..#.#.....#....##

The first nine asteroids to get vaporized, in order, would be:

.#....###24...#..
##...##.13#67..9#
##...#...5.8####.
..#.....X...###..
..#.#.....#....##

Note that some asteroids (the ones behind the asteroids marked 1, 5,
and 7) won't have a chance to be vaporized until the next full
rotation. The laser continues rotating; the next nine to be vaporized
are:

.#....###.....#..
##...##...#.....#
##...#......1234.
..#.....X...5##..
..#.9.....8....76

The next nine to be vaporized are then:

.8....###.....#..
56...9#...#.....#
34...7...........
..2.....X....##..
..1..............

Finally, the laser completes its first full rotation (1 through 3), a
second rotation (4 through 8), and vaporizes the last asteroid (9)
partway through its third rotation:

......234.....6..
......1...5.....7
.................
........X....89..
.................

In the large example above (the one with the best monitoring station
location at 11,13):

    The 1st asteroid to be vaporized is at 11,12.
    The 2nd asteroid to be vaporized is at 12,1.
    The 3rd asteroid to be vaporized is at 12,2.
    The 10th asteroid to be vaporized is at 12,8.
    The 20th asteroid to be vaporized is at 16,0.
    The 50th asteroid to be vaporized is at 16,9.
    The 100th asteroid to be vaporized is at 10,16.
    The 199th asteroid to be vaporized is at 9,6.
    The 200th asteroid to be vaporized is at 8,2.
    The 201st asteroid to be vaporized is at 10,9.
    The 299th and final asteroid to be vaporized is at 11,1.

The Elves are placing bets on which will be the 200th asteroid to be
vaporized. Win the bet by determining which asteroid that will be;
what do you get if you multiply its X coordinate by 100 and then add
its Y coordinate? (For example, 8,2 becomes 802.)
'''

import sys
import math
import collections
from point_segment import *

class Monitor(object):
    def __init__(self, pos, points):
        self.sight_map = []
        self.pos = pos

        self.points = points
        self.points_rev_idx = self.build_points_reverse_indices()

        self.vaporized_points = []
        

        for i in range(0, len(self.points)):
            r = [True] * len(self.points)
            r[i] = False # set self cannot see self
            self.sight_map.append(r)

        self.slopes_at_point = self.build_slopes()

    def build_points_reverse_indices(self):
        self.points_rev_idx = {}
        for i, p in enumerate(self.points):
            self.points_rev_idx[p] = i

        return self.points_rev_idx

            
    def build_slopes_for_all_lines():
        self.slopes_at_point = collections.defaultdict()
        for a in self.points:
            for b in self.points:
                if a != b:
                    seg = LineSegment(a, b)

                    if a in self.slopes_at_point:
                        points_follow_slopes = self.slopes_at_point[a]
                    else:
                        points_follow_slopes = collections.defaultdict(list)
                        self.slopes_at_point[a] = points_follow_slopes
                    
                    points_follow_slopes[seg.slope].append(b)

    def build_slopes(self):
        self.slopes_at_point = collections.defaultdict()
        a = self.pos
        points_follow_slopes = collections.defaultdict(list)
        self.slopes_at_point[a] = points_follow_slopes

        for b in self.points:
            if a != b:
                seg = LineSegment(a, b)
                points_follow_slopes[seg.slope].append(b)
                
        return self.slopes_at_point

    def build_sight_map(self):
        for a in self.points:
            a_idx = self.points_rev_idx[a]
        
            for b in self.points:
                b_idx = self.points_rev_idx[b]

                # The sight_map is symmetric, we update both ends
                # of a segment if both ends cannot see each other.
                # If we know they cannot see each others already,
                # skip checking
                if self.sight_map[a_idx][b_idx] == False:
                    continue
            
                if a != b:
                    seg = LineSegment(a, b)
                    points_with_same_slopes = self.slopes_at_point[a][seg.slope]
                    for p in points_with_same_slopes:
                        if p != b:
                            if seg.has_point(p):
                                self.sight_map[a_idx][b_idx] = False
                                self.sight_map[b_idx][a_idx] = False

    def visible_points_from(self, src):
        i = self.points_rev_idx[src]
        vpoints = []
        for j in range(0, len(self.points)):
            v = self.sight_map[i][j]
            if v:
                vpoints.append(self.points[j])

        return vpoints

    def visible_points(self):

        visible_from_here = [True] * len(self.points)
        
        a = self.pos
        a_idx = self.points_rev_idx[a]
        visible_from_here[a_idx] = False
        
        
        for b in self.points:
            b_idx = self.points_rev_idx[b]

            if a != b:
                seg = LineSegment(a, b)
                points_with_same_slopes = self.slopes_at_point[a][seg.slope]
                for p in points_with_same_slopes:
                    if p != b:
                        if seg.has_point(p):
                            visible_from_here[b_idx] = False

        vpoints = []
        for i in range(0, len(visible_from_here)):
            if visible_from_here[i]:
                vpoints.append(self.points[i])

        return vpoints

    def rotate(self):

        vpoints = self.visible_points()
        degresses = []
        for p in vpoints:
            tp = p.get_translate(self.pos)
            d = tp.get_degree()
            # The degress is relative to self.pos, the point data use the
            # original representation.
            degresses.append((d, p)) 

        degresses.sort(key = lambda x : x[0])

        list(map(lambda x : print("{} {}".format(x[0], x[1])), degresses))
        
        return degresses

    def laser(self):
        degresses = self.rotate()
        shoot_at = []
        remaining_points = []
        for d in degresses:
            p = d[1]
            shoot_at.append(p)
            self.vaporized_points.append(p)

        for p in self.points:
            if not p in shoot_at:
                remaining_points.append(p)

        print("remaining_points:", *remaining_points)

        self.points = remaining_points
        self.points_rev_idx = self.build_points_reverse_indices()
        self.slopes_at_point = self.build_slopes()
            
def test_case_5():
    print("Only valid with input file test5.txt, command line argv [11, 13]")
    
    check_test_5 = [0, 1, 2, 9, 19, 49, 99, 198, 199, 200, 298]
    for c in check_test_5:
        print("The {}th vaporized {}".format(c+1,
                                             mon.vaporized_points[c]))

    ## DEBUG
    o = Point(11, 13)
    p = Point(12, 19)
    print("Origin:", o)
    tp = p.get_translate(o)
    print("{} -> {}, relative deg: {}".format(p, tp, tp.get_degree()))
    p = Point(12, 7)
    tp = p.get_translate(o)
    print("{} -> {}, relative deg: {}".format(p, tp, tp.get_degree()))
    p = Point(13, 2)
    tp = p.get_translate(o)
    print("{} -> {}, relative deg: {}".format(p, tp, tp.get_degree()))
    p = Point(5, 16)
    tp = p.get_translate(o)
    print("{} -> {}, relative deg: {}".format(p, tp, tp.get_degree()))
    p = Point(5, 2)
    tp = p.get_translate(o)
    print("{} -> {}, relative deg: {}".format(p, tp, tp.get_degree()))


def main():
    points = []
    points_rev_idx = {}
    
    line = sys.stdin.readline().strip()
    y = 0
    i = 0
    while line:
        x = 0
        for p in line:
            if p == '#':
                point = Point(x, y)
                points.append(point)
                points_rev_idx[point] = i
                i += 1
            x += 1

        y += 1
        line = sys.stdin.readline().strip()

    mx = int(sys.argv[1])
    my = int(sys.argv[2])
    mon = Monitor(Point(mx, my), points)
    v = mon.visible_points()
    print("From self:", *v)


    p = Point(0, 10)
    print("p {} degree {}".format(p, p.get_degree()))
    p = Point(1, 1)
    print("p {} degree {}".format(p, p.get_degree()))
    
    p = Point(10, 0)
    print("p {} degree {}".format(p, p.get_degree()))
    p = Point(10, -5)
    print("p {} degree {}".format(p, p.get_degree()))

    p = Point(0, -10)
    print("p {} degree {}".format(p, p.get_degree()))
    p = Point(-10, -7)
    print("p {} degree {}".format(p, p.get_degree()))

    p = Point(-10, 0)
    print("p {} degree {}".format(p, p.get_degree()))
    p = Point(-10, 5)
    print("p {} degree {}".format(p, p.get_degree()))
    

    print("start rotate")
    r = 1
    while len(mon.points) > 1:
        print("--- rotate {} time ---".format(r))
        mon.laser()
        r += 1

    print("--- done ---")
    
    print("200th asteroid to be vaporized:", mon.vaporized_points[199],
          "ans:",
          (mon.vaporized_points[199].x * 100) + mon.vaporized_points[199].y)


if __name__ == "__main__":
    sys.exit(main())
