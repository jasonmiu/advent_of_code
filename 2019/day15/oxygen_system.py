#!/usr/bin/env python3
'''
--- Day 15: Oxygen System ---

Out here in deep space, many things can go wrong. Fortunately, many of
those things have indicator lights. Unfortunately, one of those lights
is lit: the oxygen system for part of the ship has failed!

According to the readouts, the oxygen system must have failed days ago
after a rupture in oxygen tank two; that section of the ship was
automatically sealed once oxygen levels went dangerously low. A single
remotely-operated repair droid is your only option for fixing the
oxygen system.

The Elves' care package included an Intcode program (your puzzle
input) that you can use to remotely control the repair droid. By
running that program, you can direct the repair droid to the oxygen
system and fix the problem.

The remote control program executes the following steps in a loop forever:

    Accept a movement command via an input instruction.
    Send the movement command to the repair droid.
    Wait for the repair droid to finish the movement operation.
    Report on the status of the repair droid via an output instruction.

Only four movement commands are understood: north (1), south (2), west
(3), and east (4). Any other command is invalid. The movements differ
in direction, but not in distance: in a long enough east-west hallway,
a series of commands like 4,4,4,4,3,3,3,3 would leave the repair droid
back where it started.

The repair droid can reply with any of the following status codes:

    0: The repair droid hit a wall. Its position has not changed.
    1: The repair droid has moved one step in the requested direction.
    2: The repair droid has moved one step in the requested direction;
    its new position is the location of the oxygen system.

You don't know anything about the area around the repair droid, but
you can figure it out by watching the status codes.

For example, we can draw the area using D for the droid, # for walls,
. for locations the droid can traverse, and empty space for unexplored
locations. Then, the initial state looks like this:

      
      
   D  
      
      

To make the droid go north, send it 1. If it replies with 0, you know
that location is a wall and that the droid didn't move:

      
   #  
   D  
      
      

To move east, send 4; a reply of 1 means the movement was successful:

      
   #  
   .D 
      
      

Then, perhaps attempts to move north (1), south (2), and east (4) are
all met with replies of 0:

      
   ## 
   .D#
    # 
      

Now, you know the repair droid is in a dead end. Backtrack with 3
(which you already know will get a reply of 1 because you already know
that location is open):

      
   ## 
   D.#
    # 
      

Then, perhaps west (3) gets a reply of 0, south (2) gets a reply of 1,
south again (2) gets a reply of 0, and then west (3) gets a reply of
2:

      
   ## 
  #..#
  D.# 
   #  

Now, because of the reply of 2, you know you've found the oxygen
system! In this example, it was only 2 moves away from the repair
droid's starting position.

What is the fewest number of movement commands required to move the
repair droid from its starting position to the location of the oxygen
system?

Your puzzle answer was 222.
--- Part Two ---

You quickly repair the oxygen system; oxygen gradually fills the area.

Oxygen starts in the location containing the repaired oxygen
system. It takes one minute for oxygen to spread to all open locations
that are adjacent to a location that already contains oxygen. Diagonal
locations are not adjacent.

In the example above, suppose you've used the droid to explore the
area fully and have the following map (where locations that currently
contain oxygen are marked O):

 ##   
#..## 
#.#..#
#.O.# 
 ###  

Initially, the only location which contains oxygen is the location of
the repaired oxygen system. However, after one minute, the oxygen
spreads to all open (.) locations that are adjacent to a location
containing oxygen:

 ##   
#..## 
#.#..#
#OOO# 
 ###  

After a total of two minutes, the map looks like this:

 ##   
#..## 
#O#O.#
#OOO# 
 ###  

After a total of three minutes:

 ##   
#O.## 
#O#OO#
#OOO# 
 ###  

And finally, the whole region is full of oxygen after a total of four minutes:

 ##   
#OO## 
#O#OO#
#OOO# 
 ###  

So, in this example, all locations contain oxygen after 4 minutes.

Use the repair droid to get a complete map of the area. How many
minutes will it take to fill with oxygen?

Your puzzle answer was 394.
'''


import sys
import intcode
import queue
import collections

class Node(object):

    TYPE_WALL = "0"
    TYPE_SPACE = "1"
    TYPE_GOAL = "2"
    TYPE_PATH = "99"
    TYPE_OXY = "100"
    DIR_N = "1"
    DIR_S = "2"
    DIR_W = "3"
    DIR_E = "4"
    DIR_START = "0"
    
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.connects = [] # list of Nodes
        self.grid_type = None

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
        
    def __hash__(self):
        return hash((self.x, self.y))

    def __str__(self):
        return "({}, {}) {}".format(self.x, self.y, self.grid_type)

class ShipMap(object):
    def __init__(self):
        self.start = None
        self.goal = None
        self.visited = {}
        self.prog = None
        self.prog_rc = None
        self.prog_pc = 0

    def print_map(self):
        min_x = 999999999
        min_y = 999999999
        max_x = -999999999
        max_y = -999999999
        width = 0
        hight = 0

        for n in self.visited:
            x = n.x
            y = n.y
            min_x = min(min_x, x)
            min_y = min(min_y, y)
            max_x = max(max_x, x)
            max_y = max(max_y, y)

        width = max_x - min_x + 1
        hight = max_y - min_y + 1

        print("min x y {} {}, max x y {} {}".format(min_x, min_y, max_x, max_y))
        
        map = []
        for i in range(0, hight):
            map.append([0] * width)

        for n in self.visited:
            x = n.x - min_x
            y = n.y - min_y
            map[y][x] = n.grid_type

        for i in range(0, hight):
            for j in range(0, width):
                s = '?'
                # Special for starting point
                if j == 0 - min_x and i == 0 - min_y:
                    s = 'S'
                elif map[i][j] == Node.TYPE_WALL:
                    s = '#'
                elif map[i][j] == Node.TYPE_SPACE:
                    s = ' '
                elif map[i][j] == Node.TYPE_GOAL:
                    s = 'G'
                elif map[i][j] == Node.TYPE_PATH:
                    s = '.'
                elif map[i][j] == Node.TYPE_OXY:
                    s = 'o'
                    
                print(s, end="")
            print("")

        

    def build_map(self, prog, start_x, start_y):
        self.prog = prog
        self.prog_rc = intcode.IntCodeRunConfig()
        self.prog_rc.no_head = True
        self.prog_rc.use_fd = False
        self.prog_rc.in_buf = queue.Queue() 
        self.prog_rc.out_buf = queue.Queue()

        self.expore(start_x, start_y, Node.DIR_START, None)
        

    def expore(self, cur_x, cur_y, from_dir, from_node):

#        print("!!!export ({}, {})".format(cur_x, cur_y))
#        print("!!!visited", list(map(str, self.visited.keys())))

        n = Node(cur_x, cur_y)
        if n not in self.visited:
            self.visited[n] = True

        # Starting point, we do not read the state from intcode prog,
        # but it must be a open space
        if from_dir == Node.DIR_START:
            self.start = n
            n.grid_type = Node.TYPE_SPACE

#        print("before read outbuf x, y:", cur_x, cur_y, "visited", list(map(str, self.visited.keys())))            
        if self.prog_rc.out_buf.qsize() > 0:
            state = self.prog_rc.out_buf.get()
            n.grid_type = state
            print("Now grid", n)
            if state == Node.TYPE_WALL:
                print("  HIT WALL")
                return

            if state == Node.TYPE_GOAL:
                print("SEE GOAL!")
                self.goal = n

        if from_dir != Node.DIR_START:
            n.connects.append(from_node)
            from_node.connects.append(n)
            
#        print("before N cur x, y:", cur_x, cur_y, "visited", list(map(str, self.visited.keys())))            
        if Node(cur_x, cur_y - 1) not in self.visited:
            print("GO N")
            print("cur x, y:", cur_x, cur_y)
            self.prog_rc.in_buf.put(Node.DIR_N)
            self.prog_pc = intcode.run(self.prog, self.prog_rc, self.prog_pc)
            self.expore(cur_x, cur_y - 1, Node.DIR_N, n)

#        print("before E cur x, y:", cur_x, cur_y, "visited", list(map(str, self.visited.keys())))            
        if Node(cur_x + 1, cur_y) not in self.visited:
            print("GO E")
            print("cur x, y:", cur_x, cur_y)
            self.prog_rc.in_buf.put(Node.DIR_E)
            self.prog_pc = intcode.run(self.prog, self.prog_rc, self.prog_pc)
            self.expore(cur_x + 1, cur_y, Node.DIR_E, n)

#        print("before S cur x, y:", cur_x, cur_y, "visited", list(map(str, self.visited.keys())))            
        if Node(cur_x, cur_y + 1) not in self.visited:
            print("GO S")
            print("cur x, y:", cur_x, cur_y)
            self.prog_rc.in_buf.put(Node.DIR_S)
            self.prog_pc = intcode.run(self.prog, self.prog_rc, self.prog_pc)
            self.expore(cur_x, cur_y + 1, Node.DIR_S, n)
            

#        print("before W cur x, y:", cur_x, cur_y, "visited", list(map(str, self.visited.keys())))
#        print(Node(cur_x - 1, cur_y) in self.visited)
        if Node(cur_x - 1, cur_y) not in self.visited:
            print("GO W")
            print("cur x, y:", cur_x, cur_y)
            self.prog_rc.in_buf.put(Node.DIR_W)
            self.prog_pc = intcode.run(self.prog, self.prog_rc, self.prog_pc)
            self.expore(cur_x - 1, cur_y, Node.DIR_W, n)

        step_back = None
        if from_dir == Node.DIR_N:
            step_back = Node.DIR_S
        elif from_dir == Node.DIR_S:
            step_back = Node.DIR_N
        elif from_dir == Node.DIR_E:
            step_back = Node.DIR_W
        elif from_dir == Node.DIR_W:
            step_back = Node.DIR_E

        if from_dir != Node.DIR_START: 
            self.prog_rc.in_buf.put(step_back)
            self.prog_pc = intcode.run(self.prog, self.prog_rc, self.prog_pc)
            state = self.prog_rc.out_buf.get()
            print("Go Back ", state)
            
        return

    def bfs(self, root, goal):
        visited = set()
        dist_to = collections.defaultdict(int)
        path_to = collections.defaultdict(list)
        
        q = queue.Queue()

        n = root
        q.put(n)
        visited.add(n)
        dist_to[n] = 0
        path_to[n] = [n]
        
        while q.qsize() > 0:
            n = q.get()

            for c in n.connects:
                if c not in visited:
                    visited.add(c)
                    q.put(c)

                # If the distance from source to current node + 1 is
                # shorter than distance from source to this child node,
                # this is shorter path to this child node.
                # If current distance from source to child node
                # is zero, means we didn't find a path to this child node
                # yet, put it to the distance from source to current node + 1.
                # We excluded the source node already, so it will not be the
                # child node. We can also init all distance to a node to
                # MAX_INT first.
                if (dist_to[n] + 1) < dist_to[c] or dist_to[c] == 0:
                    dist_to[c] = dist_to[n] + 1
                    path_to[c] = path_to[n] + [c]
                    

        return (dist_to[goal], path_to[goal])

                
    def steps_to_node(self, start, goal):
        return self.bfs(start, goal)


    # Using flood fill algorithm to fill up all connected nodes
    # For all connected nodes, we recursivly call the flood fill starting
    # for current node. The steps needed to fill up all connected nodes
    # from current node is the max steps of all children nodes, since the
    # max steps means the 'futherest' node we need to reach.
    def flood_fill(self, start):
        if start.grid_type == Node.TYPE_OXY or start.grid_type == Node.TYPE_WALL:
            return 0

        start.grid_type = Node.TYPE_OXY
        steps = []
        for c in start.connects:
            steps.append(self.flood_fill(c))
        return max(steps) + 1
        
    def steps_to_fill(self, start):
        # We counted the starting point with 1. So -1 for that.
        return self.flood_fill(start) - 1
        
    

def main():

    if len(sys.argv) != 2:
        print("{} <intcode prog file>".format(sys.argv[0]))
        return 1
    
    with open(sys.argv[1], 'r') as fd:
        line = fd.readline().strip()
        prog = line.split(',')

    prog_ex = [0] * (len(prog) * 10)
# For interactive run the maze
#    prog.extend(prog_ex)
#    intcode.run(prog)

    sm = ShipMap()
    sm.build_map(prog, 0, 0)
    sm.print_map()

    print("START node", sm.start, "START connections", sm.start.connects)
    (shortest, path) = sm.steps_to_node(sm.start, sm.goal)

    print("shortest path to {} is {}".format(sm.goal, shortest))
    for n in path:
        n.grid_type = Node.TYPE_PATH
    path[-1].grid_type = Node.TYPE_GOAL
    sm.print_map()

    oxygen_time = sm.steps_to_fill(sm.goal)
    print("Time to fill up oxygen:", oxygen_time)

if __name__ == "__main__":
    sys.exit(main())
