def wire_to_segments(wire):
    segs = []

    x = y = 0

    for s in wire:
        d = s[0]
        steps = int(s[1::])

        if d == "U":
            seg = ((x, y), (x, y + steps))
            y += steps
        elif d == "D":
            seg = ((x, y), (x, y - steps))
            y -= steps
        elif d == "L":
            seg = ((x, y), (x - steps, y))
            x -= steps
        elif d == "R":
            seg = ((x, y), (x + steps, y))
            x += steps

        segs.append(seg)

    return segs

def steps_to_point(wire, dest):
    x = y = 0
    total_steps = 0
    
    for s in wire:
        d = s[0]
        steps = int(s[1::])

        if d == "U":
            seg = ((x, y), (x, y + steps))
            y += steps
        elif d == "D":
            seg = ((x, y), (x, y - steps))
            y -= steps
        elif d == "L":
            seg = ((x, y), (x - steps, y))
            x -= steps
        elif d == "R":
            seg = ((x, y), (x + steps, y))
            x += steps

        total_steps += steps

        c = segment_cross(seg, (dest, dest))
        if c:
            s = abs(c[0] - seg[0][0]) + abs(c[1] - seg[0][1])
            total_steps = total_steps - steps + s
            break
        

    return total_steps


def segment_cross(s1, s2):
    # If x1 and x2 of a segment is the same,
    # it is vertical.
    if s1[0][0] == s1[1][0]:
        v_seg = s1
        h_seg = s2
    else:
        v_seg = s2
        h_seg = s1

    v_seg_x = v_seg[0][0]
    h_seg_y = h_seg[0][1]

    # corss at the starting origin, not count
    if v_seg[0][0] == 0 and v_seg[0][1] == 0 and \
       h_seg[0][0] == 0 and h_seg[0][1] == 0:
        return None

    if v_seg_x >= min(h_seg[0][0], h_seg[1][0]) and \
       v_seg_x <= max(h_seg[0][0], h_seg[1][0]) and \
       h_seg_y >= min(v_seg[0][1], v_seg[1][1]) and \
       h_seg_y <= max(v_seg[0][1], v_seg[1][1]):
        return (v_seg[0][0], h_seg[0][1])
    else:
        return None
