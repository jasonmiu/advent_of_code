#!/usr/bin/env python3

import sys
from wire import *

def main():
    line = sys.stdin.readline().strip()
    wire1 = line.split(',')
    line = sys.stdin.readline().strip()
    wire2 = line.split(',')

    steps = steps_to_point(wire1, (3, 3))
    print("wire 1 steps to (3, 3)", steps)

    steps = steps_to_point(wire1, (6, 5))
    print("wire 1 steps to (6, 5)", steps)

    segs1 = wire_to_segments(wire1)
    segs2 = wire_to_segments(wire2)

    min_steps = 9999999999
    min_steps_cross = None
    crosses = []
    for s1 in segs1:
        for s2 in segs2:
            c = segment_cross(s1, s2)
            if c:
                crosses.append(c)
                steps_1 = steps_to_point(wire1, c)
                steps_2 = steps_to_point(wire2, c)
                steps = steps_1 + steps_2
                if steps < min_steps:
                    min_steps = steps
                    min_steps_cross = c
                
    print("Min steps intersection {}, min steps {}".format(min_steps_cross, min_steps))

    return 0



if __name__ == "__main__":
    sys.exit(main())
