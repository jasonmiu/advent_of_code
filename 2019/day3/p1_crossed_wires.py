#!/usr/bin/env python3

'''
--- Day 3: Crossed Wires ---

The gravity assist was successful, and you're well on your way to the
Venus refuelling station. During the rush back on Earth, the fuel
management system wasn't completely installed, so that's next on the
priority list.

Opening the front panel reveals a jumble of wires. Specifically, two
wires are connected to a central port and extend outward on a
grid. You trace the path each wire takes as it leaves the central
port, one wire per line of text (your puzzle input).

The wires twist and turn, but the two wires occasionally cross
paths. To fix the circuit, you need to find the intersection point
closest to the central port. Because the wires are on a grid, use the
Manhattan distance for this measurement. While the wires do
technically cross right at the central port where they both start,
this point does not count, nor does a wire count as crossing with
itself.

For example, if the first wire's path is R8,U5,L5,D3, then starting
from the central port (o), it goes right 8, up 5, left 5, and finally
down 3:

...........
...........
...........
....+----+.
....|....|.
....|....|.
....|....|.
.........|.
.o-------+.
...........

Then, if the second wire's path is U7,R6,D4,L4, it goes up 7, right 6,
down 4, and left 4:

...........
.+-----+...
.|.....|...
.|..+--X-+.
.|..|..|.|.
.|.-X--+.|.
.|..|....|.
.|.......|.
.o-------+.
...........

These wires cross at two locations (marked X), but the lower-left one
is closer to the central port: its distance is 3 + 3 = 6.

Here are a few more examples:

    R75,D30,R83,U83,L12,D49,R71,U7,L72 U62,R66,U55,R34,D71,R55,D58,R83
    = distance 159 R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
    U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = distance 135

What is the Manhattan distance from the central port to the closest
intersection?

'''


import sys

def wire_to_segments(wire):
    segs = []
    f_results = []

    x = y = 0

    for s in wire:
        d = s[0]
        steps = int(s[1::])

        if d == "U":
            seg = ((x, y), (x, y + steps))
            y += steps
        elif d == "D":
            seg = ((x, y), (x, y - steps))
            y -= steps
        elif d == "L":
            seg = ((x, y), (x - steps, y))
            x -= steps
        elif d == "R":
            seg = ((x, y), (x + steps, y))
            x += steps

        segs.append(seg)

    return segs


def segment_cross(s1, s2):
    # If x1 and x2 of a segment is the same,
    # it is vertical.
    if s1[0][0] == s1[1][0]:
        v_seg = s1
        h_seg = s2
    else:
        v_seg = s2
        h_seg = s1

    v_seg_x = v_seg[0][0]
    h_seg_y = h_seg[0][1]

    # corss at the starting origin, not count
    if v_seg[0][0] == 0 and v_seg[0][1] == 0 and \
       h_seg[0][0] == 0 and h_seg[0][1] == 0:
        return None

    if v_seg_x >= min(h_seg[0][0], h_seg[1][0]) and \
       v_seg_x <= max(h_seg[0][0], h_seg[1][0]) and \
       h_seg_y >= min(v_seg[0][1], v_seg[1][1]) and \
       h_seg_y <= max(v_seg[0][1], v_seg[1][1]):
        return (v_seg[0][0], h_seg[0][1])
    else:
        return None

def _test(wire1, wire2):
    segs = wire_to_segments(wire1, None)
    print(segs)

    segs = wire_to_segments(wire2, None)
    print(segs)

    c = segment_cross(((3, 5), (3, 2)),
                      ((6, 3), (2, 3)))
    print(c)

    c = segment_cross(((8, 0), (8, 5)),
                      ((0, 7), (6, 7)))
    print(c)
    

def main():
    line = sys.stdin.readline().strip()
    wire1 = line.split(',')
    line = sys.stdin.readline().strip()
    wire2 = line.split(',')

    segs1 = wire_to_segments(wire1)
    segs2 = wire_to_segments(wire2)

    crosses = []
    closest = 999999999999999
    closest_point = None
    for s1 in segs1:
        for s2 in segs2:
            c = segment_cross(s1, s2)
            if c:
                crosses.append(c)
                d = abs(c[0]) + abs(c[1])
                if d < closest:
                    closest = d
                    closest_point = c

    print(crosses)
    print("Closest intersection {}, closest intersection Manhattan distance {}".\
          format(closest_point, closest))
    

if __name__ == "__main__":
    sys.exit(main())
