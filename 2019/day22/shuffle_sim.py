#!/usr/bin/env python3

import parse_input
import sys


CARD_NR = 10


def new_deck(n):
    d = [i for i in range(0, n)]

    return d

def deal_stack(deck):
    return list(reversed(deck))

def cut_cards(deck, n):
    d1 = deck[0:n]
    d2 = deck[n::]
    return d2 + d1

def deal_increment(deck, n):
    table = [0] * len(deck)
    for i in range(0, len(deck)):
        
        ti = i * n
        if ti > n-1:
            ti = ti % len(deck)
            
        table[ti] = deck[i]

    return table

def shuffle(deck, instructions):
    d = deck
    for s in instructions:
        if s[0] == "N":
            d = deal_stack(d)
        elif s[0] == "I":
            d = deal_increment(d, s[1])
        elif s[0] == "C":
            d = cut_cards(d, s[1])

    return d
            


if __name__ == "__main__":
    
    lines = sys.stdin.readlines()

    insts = parse_input.strs_to_instructions(lines)

    d = new_deck(CARD_NR)
    nd = deal_stack(d)
    print(nd)

    d = new_deck(CARD_NR)
    nd = cut_cards(d, 3)
    print(nd)

    d = new_deck(CARD_NR)
    nd = cut_cards(d, -4)
    print(nd)

    d = new_deck(CARD_NR)
    nd = deal_increment(d, 3)
    print(nd)

    print("--------------------")
    
    d = new_deck(CARD_NR)
    d = shuffle(d, insts)
    print(d)

    sys.exit(0)
