#!/usr/bin/env python3

'''
--- Part Two ---

After a while, you realize your shuffling skill won't improve much
more with merely a single deck of cards. You ask every 3D printer on
the ship to make you some more cards while you check on the ship
repairs. While reviewing the work the droids have finished so far, you
think you see Halley's Comet fly past!

When you get back, you discover that the 3D printers have combined
their power to create for you a single, giant, brand new, factory
order deck of 119315717514047 space cards.

Finally, a deck of cards worthy of shuffling!

You decide to apply your complete shuffle process (your puzzle input)
to the deck 101741582076661 times in a row.

You'll need to be careful, though - one wrong move with this many
cards and you might overflow your entire ship!

After shuffling your new, giant, factory order deck that many times,
what number is on the card that ends up in position 2020?

Your puzzle answer was 70618172909245.

'''

# Ref: https://www.reddit.com/r/adventofcode/comments/ee0rqi/2019_day_22_solutions/fbnifwk/
# Ref: https://przybyl.io/solution-explanation-to-day-22-of-advent-of-code-2019.html

import sys
import argparse
import parse_input


def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m

# For the position i, what is the card id at i after
# deal a stack?
def r_deal(card_nr, i):
    return card_nr - 1 - i;

# For the position i, what is the card id at i after
# cutting <cut> cards?
def r_cut_cards(card_nr, i, cut):
    c = (i + cut + card_nr) % card_nr
    return c

# For the position i, what is the card id at i after
# dealing increment <inc>?
def r_deal_increment(card_nr, i, inc):
    c = modinv(inc, card_nr) * i % card_nr
    return c


def shuffle_once(card_nr, pos, insts):
    p = pos
    
    for s in reversed(insts):
        if s[0] == "N":
            p = r_deal(card_nr, p)
        elif s[0] == "I":
            p = r_deal_increment(card_nr, p, s[1])
        elif s[0] == "C":
            p = r_cut_cards(card_nr, p, s[1])

    return p

def linear(card_nr, pos, insts):
    x = pos
    y = shuffle_once(card_nr, x, insts)
    z = shuffle_once(card_nr, y, insts)

    a = (y-z) * modinv(x - y + card_nr, card_nr) % card_nr
    b = (y - (a * x)) % card_nr

    return (a, b)

def resolve(card_nr, pos, insts, shuffle_time):
    (a, b) = linear(card_nr, pos, insts)

    # f^n(x) = A^n*x + A^(n-1)*B + A^(n-2)*B + ... + B
    #    = A^n*x + (A^(n-1) + A^(n-2) + ... + 1) * B
    #    = A^n*x + (A^n-1) / (A-1) * B
    x = pos
    n = shuffle_time

    ans = ((pow(a, n, card_nr) * x) + \
           (pow(a, n, card_nr) - 1) * modinv(a-1, card_nr) * b) % \
           card_nr
    return ans
    


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument('-n', action="store", required=False,
                    help="Number of the cards in the deck")
    ap.add_argument('-p', action="store", required=False,
                    help="Position of the deck we want to look")
    ap.add_argument('-s', action="store", required=False,
                    help="Number of the shuffles")


    args = ap.parse_args(sys.argv[1::])

    if args.n:
        card_nr = int(args.n)
    else:
        card_nr = 119315717514047

    if args.p:
        deck_pos = int(args.p)
    else:
        deck_pos = 2020

    if args.s:
        n = int(args.s)
    else:
        n = 101741582076661        

    print("card_nr", card_nr, "deck_pos", deck_pos)

    lines = sys.stdin.readlines()

    insts = parse_input.strs_to_instructions(lines)

    p = shuffle_once(card_nr, deck_pos, insts)
    print(p)

    print("ans: {}".format(resolve(card_nr, deck_pos, insts, n)))


    


if __name__ == "__main__":
    sys.exit(main())
