#!/usr/bin/env python3

"""
"N" is deal into new stack
"I" is deal with increment
"C" is cut
"""

def strs_to_instructions(strs):

    instructions = []
    
    for s in strs:
        ins = []
        if s.startswith("deal with"):
            toks = s.split()
            ins.append("I")
            ins.append(int(toks[3]))
        elif s.startswith("deal into"):
            toks = s.split()
            ins.append("N")
        elif s.startswith("cut"):
            toks = s.split()
            ins.append("C")
            ins.append(int(toks[1]))

        instructions.append(ins)

    return instructions

if __name__ == "__main__":
    import sys
    lines = sys.stdin.readlines()

    insts = strs_to_instructions(lines)

    for i in insts:
        print(i)

    sys.exit(0)
        
        
        
