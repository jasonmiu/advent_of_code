#!/usr/bin/env python3

'''
--- Day 17: Set and Forget ---

An early warning system detects an incoming solar flare and
automatically activates the ship's electromagnetic
shield. Unfortunately, this has cut off the Wi-Fi for many small
robots that, unaware of the impending danger, are now trapped on
exterior scaffolding on the unsafe side of the shield. To rescue them,
you'll have to act quickly!

The only tools at your disposal are some wired cameras and a small
vacuum robot currently asleep at its charging station. The video
quality is poor, but the vacuum robot has a needlessly bright LED that
makes it easy to spot no matter where it is.

An Intcode program, the Aft Scaffolding Control and Information
Interface (ASCII, your puzzle input), provides access to the cameras
and the vacuum robot. Currently, because the vacuum robot is asleep,
you can only access the cameras.

Running the ASCII program on your Intcode computer will provide the
current view of the scaffolds. This is output, purely coincidentally,
as ASCII code: 35 means #, 46 means ., 10 starts a new line of output
below the current one, and so on. (Within a line, characters are drawn
left-to-right.)

In the camera output, # represents a scaffold and . represents open
space. The vacuum robot is visible as ^, v, <, or > depending on
whether it is facing up, down, left, or right respectively. When drawn
like this, the vacuum robot is always on a scaffold; if the vacuum
robot ever walks off of a scaffold and begins tumbling through space
uncontrollably, it will instead be visible as X.

In general, the scaffold forms a path, but it sometimes loops back
onto itself. For example, suppose you can see the following view from
the cameras:

..#..........
..#..........
#######...###
#.#...#...#.#
#############
..#...#...#..
..#####...^..

Here, the vacuum robot, ^ is facing up and sitting at one end of the
scaffold near the bottom-right of the image. The scaffold continues
up, loops across itself several times, and ends at the top-left of the
image.

The first step is to calibrate the cameras by getting the alignment
parameters of some well-defined points. Locate all scaffold
intersections; for each, its alignment parameter is the distance
between its left edge and the left edge of the view multiplied by the
distance between its top edge and the top edge of the view. Here, the
intersections from the above image are marked O:

..#..........
..#..........
##O####...###
#.#...#...#.#
##O###O###O##
..#...#...#..
..#####...^..

For these intersections:

    The top-left intersection is 2 units from the left of the image
    and 2 units from the top of the image, so its alignment parameter
    is 2 * 2 = 4.  The bottom-left intersection is 2 units from the
    left and 4 units from the top, so its alignment parameter is 2 * 4
    = 8.  The bottom-middle intersection is 6 from the left and 4 from
    the top, so its alignment parameter is 24.  The bottom-right
    intersection's alignment parameter is 40.

To calibrate the cameras, you need the sum of the alignment
parameters. In the above example, this is 76.

Run your ASCII program. What is the sum of the alignment parameters
for the scaffold intersections?

Your puzzle answer was 8520.
--- Part Two ---

Now for the tricky part: notifying all the other robots about the
solar flare. The vacuum robot can do this automatically if it gets
into range of a robot. However, you can't see the other robots on the
camera, so you need to be thorough instead: you need to make the
vacuum robot visit every part of the scaffold at least once.

The vacuum robot normally wanders randomly, but there isn't time for
that today. Instead, you can override its movement logic with new
rules.

Force the vacuum robot to wake up by changing the value in your ASCII
program at address 0 from 1 to 2. When you do this, you will be
automatically prompted for the new movement rules that the vacuum
robot should use. The ASCII program will use input instructions to
receive them, but they need to be provided as ASCII code; end each
line of logic with a single newline, ASCII code 10.

First, you will be prompted for the main movement routine. The main
routine may only call the movement functions: A, B, or C. Supply the
movement functions to use as ASCII text, separating them with commas
(,, ASCII code 44), and ending the list with a newline (ASCII code
10). For example, to call A twice, then alternate between B and C
three times, provide the string A,A,B,C,B,C,B,C and then a newline.

Then, you will be prompted for each movement function. Movement
functions may use L to turn left, R to turn right, or a number to move
forward that many units. Movement functions may not call other
movement functions. Again, separate the actions with commas and end
the list with a newline. For example, to move forward 10 units, turn
left, move forward 8 units, turn right, and finally move forward 6
units, provide the string 10,L,8,R,6 and then a newline.

Finally, you will be asked whether you want to see a continuous video
feed; provide either y or n and a newline. Enabling the continuous
video feed can help you see what's going on, but it also requires a
significant amount of processing power, and may even cause your
Intcode computer to overheat.

Due to the limited amount of memory in the vacuum robot, the ASCII
definitions of the main routine and the movement functions may each
contain at most 20 characters, not counting the newline.

For example, consider the following camera feed:

#######...#####
#.....#...#...#
#.....#...#...#
......#...#...#
......#...###.#
......#.....#.#
^########...#.#
......#.#...#.#
......#########
........#...#..
....#########..
....#...#......
....#...#......
....#...#......
....#####......

In order for the vacuum robot to visit every part of the scaffold at
least once, one path it could take is:

R,8,R,8,R,4,R,4,R,8,L,6,L,2,R,4,R,4,R,8,R,8,R,8,L,6,L,2

Without the memory limit, you could just supply this whole string to
function A and have the main routine call A once. However, you'll need
to split it into smaller parts.

One approach is:

    Main routine: A,B,C,B,A,C
    (ASCII input: 65, 44, 66, 44, 67, 44, 66, 44, 65, 44, 67, 10)
    Function A:   R,8,R,8
    (ASCII input: 82, 44, 56, 44, 82, 44, 56, 10)
    Function B:   R,4,R,4,R,8
    (ASCII input: 82, 44, 52, 44, 82, 44, 52, 44, 82, 44, 56, 10)
    Function C:   L,6,L,2
    (ASCII input: 76, 44, 54, 44, 76, 44, 50, 10)

Visually, this would break the desired path into the following parts:

A,        B,            C,        B,            A,        C
R,8,R,8,  R,4,R,4,R,8,  L,6,L,2,  R,4,R,4,R,8,  R,8,R,8,  L,6,L,2

CCCCCCA...BBBBB
C.....A...B...B
C.....A...B...B
......A...B...B
......A...CCC.B
......A.....C.B
^AAAAAAAA...C.B
......A.A...C.B
......AAAAAA#AB
........A...C..
....BBBB#BBBB..
....B...A......
....B...A......
....B...A......
....BBBBA......

Of course, the scaffolding outside your ship is much more complex.

As the vacuum robot finds other robots and notifies them of the
impending solar flare, it also can't help but leave them squeaky
clean, collecting any space dust it finds. Once it finishes the
programmed set of movements, assuming it hasn't drifted off into
space, the cleaning robot will return to its docking station and
report the amount of space dust it collected as a large, non-ASCII
value in a single output instruction.

After visiting every part of the scaffold at least once, how much dust
does the vacuum robot report it has collected?

Your puzzle answer was 926819.
'''

import sys
import intcode
import queue

class Cam(object):
    def __init__(self, prog):
        self.prog = prog
        self.prog_rc = intcode.IntCodeRunConfig()
        self.prog_rc.in_buf = queue.Queue()
        self.prog_rc.out_buf = queue.Queue()
        self.prog_rc.no_head = True
        self.prog_rc.use_fd = False
        self.prog_pc = 0

        self.image = None
        self.image_h = 0
        self.image_w = 0

        self.v_map = None
        self.v_map_start = None

    def build_image(self):
        self.image = []
        self.image.append([])
        row = 0
        intcode.run(self.prog, self.prog_rc)
        while self.prog_rc.out_buf.qsize() > 0:
            i = int(self.prog_rc.out_buf.get())
            print(chr(i), end="")
            if i != 10:
                self.image[row].append(i)
            else:
                self.image.append([])
                row += 1

        # Robot output 2 more '\n'
        del self.image[-1]
        del self.image[-1]

        self.image_h = len(self.image)
        self.image_w = len(self.image[0])

    # extra surrounding of the boundaries of image
    # the intention is to find vertices for graph algor.
    # But indeed this is not a graph problem. Sigh.
    def build_vmap(self):
        self.v_map = []
        for r in self.image:
            row = r.copy()
                    
            row.insert(0, 46)
            row.append(46)
            self.v_map.append(row)
            

        self.v_map.insert(0, ([46] * self.image_w))
        self.v_map[0].insert(0, 46)
        self.v_map[0].append(46)
        
        self.v_map.append(([46] * self.image_w))
        self.v_map[-1].insert(0, 46)
        self.v_map[-1].append(46)



    def print_vmap(self, marks=None, ascii_code=None):
        bot_marks = [ord('^'), ord('v'), ord('<'), ord('>')]
        for r in range(0, len(self.v_map)):
            for c in range(0, len(self.v_map[0])):
                g = self.v_map[r][c]

                if g in bot_marks:
                    self.v_map[r][c] = ord('#')
                    self.v_map_start = (c, r)
                
                if marks and (c, r) in marks:
                    s = chr(ascii_code)
                else:
                    s = chr(g)
                    
                print(s, end="")
            print("")
            

    def print_image(self):
        for r in range(0, len(self.image)):
            for c in range(0, len(self.image[0])):
                print(chr(self.image[r][c]), end="")
            print("")

    def find_intersections(self):
        inters = []
        for r in range(0, self.image_h):
            for c in range(0, self.image_w):
                p = self.image[r][c]
                if p == 35: # the '#'
                    n = s = e = w = 0
                    if r > 0 and r < (self.image_h - 1):
                        n = self.image[r-1][c]
                        s = self.image[r+1][c]
                    if c > 0 and c < (self.image_w - 1):
                        w = self.image[r][c-1]
                        e = self.image[r][c+1]

                    if n == s == e == w == 35:
                        inters.append((c, r)) # Use X,Y coordiation
                        # Mark for debug
                        #self.image[r][c] = ord('O')

        return inters

    def get_patch(self, r, c):
        p = [ [0] * 3,
              [0] * 3,
              [0] * 3 ]

        offsets = [(-1, -1), (-1, 0), (-1, 1),
                   (0, -1), (0, 0), (0, 1),
                   (1, -1), (1, 0), (1, 1)]

        pr = pc = 1

        for i in offsets:
            p[pr+i[0]][pc+i[1]] = self.v_map[r+i[0]][c+i[1]]

        return p

    def and_mask(self, patch, mask):
        result = [ [0] * 3,
                   [0] * 3,
                   [0] * 3 ]

        for i in range(0, 3):
            for j in range(0, 3):
                result[i][j] = patch[i][j] & mask[i][j]

        return result
        
    
    def find_vertices(self):
        v_mask = [ [ 0, 1, 0 ],
                   [ 0, 1, 0 ],
                   [ 0, 1, 0 ] ]

        h_mask = [ [ 0, 0, 0 ],
                   [ 1, 1, 1 ],
                   [ 0, 0, 0 ] ]

        cross_mask = [ [0, 1, 0],
                       [1, 1, 1],
                       [0, 1, 0] ]
                    
        vertices = []

        for r in range(1, len(self.v_map)-1):
            for c in range(1, len(self.v_map[0])-1):

                g = self.v_map[r][c]
                if g != 35:
                    continue

                p = self.get_patch(r, c)
                c_masked = self.and_mask(p, cross_mask)

                if c_masked != v_mask and c_masked != h_mask:
                    vertices.append((c, r))
                
        return vertices

    def find_corners(self):
        c1_mask = [ [ 0, 1, 0 ],
                    [ 1, 1, 0 ],
                    [ 0, 0, 0 ] ]

        c2_mask = [ [ 0, 1, 0 ],
                    [ 0, 1, 1 ],
                    [ 0, 0, 0 ] ]

        c3_mask = [ [ 0, 0, 0 ],
                    [ 1, 1, 0 ],
                    [ 0, 1, 0 ] ]

        c4_mask = [ [ 0, 0, 0 ],
                    [ 0, 1, 1 ],
                    [ 0, 1, 0 ] ]

        cross_mask = [ [0, 1, 0],
                       [1, 1, 1],
                       [0, 1, 0] ]

        corners = []

        for r in range(1, len(self.v_map)-1):
            for c in range(1, len(self.v_map[0])-1):

                g = self.v_map[r][c]
                if g != 35:
                    continue

                p = self.get_patch(r, c)

                c_masked = self.and_mask(p, cross_mask)
                if c_masked == cross_mask:
                    # intersection, ignore
                    continue
                
                c_masked = self.and_mask(p, c1_mask)
                if c_masked == c1_mask:
                    corners.append((c, r))
                    continue

                c_masked = self.and_mask(p, c2_mask)
                if c_masked == c2_mask:
                    corners.append((c, r))
                    continue

                c_masked = self.and_mask(p, c3_mask)
                if c_masked == c3_mask:
                    corners.append((c, r))
                    continue

                c_masked = self.and_mask(p, c4_mask)
                if c_masked == c4_mask:
                    corners.append((c, r))
                    continue
                
        return corners
                 

class Graph(object):
    INF = 9999999
    
    def __init__(self, v_map, vertices, corners):
        self.vertices = vertices
        self.corners = corners
        self.edges = [] # adjacency matrix
        self.vertices_idx = {}
        self.v_map = v_map

        n  = len(vertices)

        for i in range(0, n): 
            r = [Graph.INF] * n
            r[i] = 0
            self.edges.append(r)

        for (i, v) in enumerate(vertices):
            self.vertices_idx[v] = i

    def find_edges(self, v_map, path_ascii):

        directions = [(0, -1), # N
                      (0, 1), # S
                      (-1, 0), # W
                      (1, 0) # E
                      ]
        for v in self.vertices:
            for d in directions:
                has_path = True
                step = 0
                cx = v[0]
                cy = v[1]
                while has_path:
                    x = cx + d[0]
                    y = cy + d[1]
                    next_grid = v_map[y][x]
                    if next_grid == path_ascii:
                        step += 1
                        cx = x
                        cy = y
#                        v_map[y][x] = ord('*')
                    else:
                        has_path = False

                    if (x, y) in self.vertices and (x, y) != v:
                        v1_idx = self.vertices_idx[v]
                        v2_idx = self.vertices_idx[(x,y)]
                        self.edges[v1_idx][v2_idx] = step
                        has_path = False

        return self.edges

    def print_edges(self):
        edges = self.edges
        print("Edges:")
        print("   ", end='')
        for i in range(0, len(edges)):
            print("{:2}".format(i), end='')
        print("")
        
        for i in range(0, len(edges)):
            print("{:2} ".format(i), end='')
            for j in range(0, len(edges)):
                s = '--' if edges[i][j] == Graph.INF else edges[i][j]
                print("{:2}".format(s), end="")
            print("")

    FACE_DIR_N = 0
    FACE_DIR_S = 1
    FACE_DIR_W = 2
    FACE_DIR_E = 3

    # Core of finding the path. Turn the direction to the path,
    # and follow to the end of the path, then trun at corners.
    # It turns out the it is a end-to-end path without any loop back
    # or branches. Not a travelling sales man or chinese post man
    # problem.
    def path_follow(self, start, start_facing, intersections):

        directions = [(0, -1), # N
                      (0, 1), # S
                      (-1, 0), # W
                      (1, 0) # E
                      ]

        dir_degs = {Graph.FACE_DIR_N:0,
                    Graph.FACE_DIR_S:180,
                    Graph.FACE_DIR_E:90,
                    Graph.FACE_DIR_W:270}

        face_symbol = {Graph.FACE_DIR_N: '^',
                       Graph.FACE_DIR_S: 'v',
                       Graph.FACE_DIR_E: '>',
                       Graph.FACE_DIR_W: '<'}

        path = ord('#')

        def get_facing_grid(pos, facing):
            d = directions[facing]
            cx = pos[0]
            cy = pos[1]
            g = self.v_map[cy + d[1]][cx + d[0]]
            print("GFG", cx, cy, "g:", chr(g), d, face_symbol[facing])

            return g

        def turning(old_dir, new_dir):
            old_facing = dir_degs[old_dir]
            new_facing = dir_degs[new_dir]
            
            if old_dir == Graph.FACE_DIR_N and new_dir == Graph.FACE_DIR_W:
                return 'L'

            if old_dir == Graph.FACE_DIR_W and new_dir == Graph.FACE_DIR_N:
                return 'R'

            if (new_facing - old_facing) > 0:
                return 'R'
            else:
                return 'L'

        seqs = []
        path_end = False
        cur_facing = start_facing
        cur_pos = start
        print("START", start, "CUR_FACING", cur_facing)
        while not path_end:
            g = get_facing_grid(cur_pos, cur_facing)
            f = cur_facing
            tried_dir = 0
            while g != path and tried_dir < 4:
                f = (f + 1) % 4
                tried_dir += 1
                g = get_facing_grid(cur_pos, f)

            new_facing = f
            print("NEW_FACING", new_facing)
            
            if tried_dir >= 4:
                path_end = True
                break

            has_path = True
            steps = 0
            cx = cur_pos[0]
            cy = cur_pos[1]
            d = directions[new_facing]
            while has_path:
                g = get_facing_grid((cx, cy), new_facing)
                if g == path:
                    cx = cx + d[0]
                    cy = cy + d[1]
                    steps += 1
                    if (cx, cy) not in intersections:
                        self.v_map[cy][cx] = ord('@')
                else:
                    has_path = False

            seqs.append((turning(cur_facing, new_facing), steps))
            cur_pos = (cx, cy)
            cur_facing = new_facing
            print("End segment. ({}), run steps: {}, cur_facing: {}".\
                  format(cur_pos, steps, face_symbol[cur_facing]))

        print("({}, {}), face: {}".format(cx, cy, face_symbol[cur_facing]))

        return seqs


def part1_ans(inters):
    s = 0
    for i in inters:
        s += i[0] * i[1]

    return s

def trans_image_intersections_to_vmap(inters):
    return [(x+1, y+1) for (x, y) in inters]

def trans_seqs_to_str_list(seqs):
    char = ord('A')
    s = []
    d = {}
    rev_d = {}
    for q in seqs:
        if q not in d:
            d[q] = char
            char += 1
            print("{} -> {}".format(q, chr(d[q])))
            rev_d[d[q]] = q

        s.append(chr(d[q]))

    return s, rev_d

def trans_bot_funs_to_seq(bot_funs, seq_rev_d):
    bot_fun_seq = {}

    for k in bot_funs:
        v = bot_funs[k]
        bot_fun_seq[k] = []
        for e in v:
            bot_fun_seq[k].append(seq_rev_d[ord(e)])
    

    return bot_fun_seq

def trans_seq_to_bot_main_fun(encoded_seqs, bot_functions):
    main = []

    rev_bot_func = {}
    longest_bot_func_len = 0
    for k in bot_functions:
        v = bot_functions[k]
        rev_bot_func[v] = k
        if len(v) > longest_bot_func_len:
            longest_bot_func_len = len(v)
    
    i = 0
    n = len(encoded_seqs)
    while i < n:
        for j in range(0, longest_bot_func_len):
            s = "".join(encoded_seqs[i:i+j+1])
            if s in rev_bot_func:
                main.append(rev_bot_func[s])
                i = i + j + 1
                break

    return main

def gen_robot_input(bot_main_fun, bot_func_seq):
    rinput = []
    input_main = []
    for c in bot_main_fun:
        input_main.append(ord(c.upper()))
        input_main.append(ord(','))
    del input_main[-1]
    input_main.append(ord('\n'))
    rinput.append(input_main)

    for k in bot_func_seq:
        v = bot_func_seq[k]
        f = []
        for s in v:
            f.append(ord(s[0]))
            f.append(ord(','))
            stepstr = "{}".format(s[1])
            for si in stepstr:
                f.append(ord(si))
            f.append(ord(','))

        del f[-1]
        f.append(ord('\n'))
        rinput.append(f)

    no_video = []
    no_video.append(ord('n'))
    no_video.append(ord('\n'))
    rinput.append(no_video)
    out = []
    for r in rinput:
        for s in r:
            out.append(str(s))
    
    print(rinput)
    
    return out
        
    
# Trying to compress the sequences with
# longest repeated substring algor.
# But this algor cannot find all repeated
# patterns which need to fill up the whole original
# input string
def longest_common_prefix(s1, s2):
    n = min(len(s1), len(s2))

    for i in range(0, n):
        if s1[i] != s2[i]:
            return s1[0:i]

    return s1[0:n]

def longest_repeated_substring(s):
    suffix_strings = []
    n = len(s)
    for i in range(0, n):
        suf = "".join(s[i:n])
        suffix_strings.append(suf)

    suffix_strings.sort()
#    for i in range(0, n):
#        print(suffix_strings[i])
    
    max_lcp_len = 0
    max_lcp = None
    for i in range(0, n-1):
        cp = longest_common_prefix(suffix_strings[i], suffix_strings[i+1])
        if len(cp) > max_lcp_len and \
           abs(len(suffix_strings[i]) - len(suffix_strings[i+1])) > len(cp):
            max_lcp_len = len(cp)
            max_lcp = cp

    return max_lcp

def group_seq_str_with_lrs(encoded_seqs, lrs, map_to, map_dict):

    map_dict[lrs] = map_to
    s = "".join(encoded_seqs)
    return s.replace(lrs, "-")

# Try find all repeated substrs.
# This is closest but cannot exclude some
# repeated substrs that cannot form a combination
# to fill up the original strings
# Using a expendable sliding window to search each patterns
def all_repeated_substr(encoded_seqs, min_w, max_w):
    i = 0
    n = len(encoded_seqs)
    s = encoded_seqs

    repeated = []
    
    while i < n:
        for w in range(min_w, max_w+1):
            j = i + w
            while j < n:
                s1 = s[i:w+i]
                s2 = s[j:w+j]
                                
                if s1 == s2:
                    repeated.append(s1)
                j += 1
        i += 1

    ans = []
    rm_idx = []
    # Try to exclude some substr which are
    # subset of others
    for i in range(0, len(repeated)):
        for j in range(0, len(repeated)):
            if i == j:
                continue
            
            r1 = "".join(repeated[i])
            r2 = "".join(repeated[j])
            if r1 != r2 and r1 in r2:
                rm_idx.append(i)

    for i in range(0, len(repeated)):
        if i not in rm_idx:
            ans.append(repeated[i])

    return ans

### ---------- find all robot subroutine algor -------
import collections
# Find all subseqs. We know the function cannot over 20 bytes
# So the search window sizes are 3 to 5 sequence elementes
def repeated_subseqs(encoded_seqs, min_w, max_w):
    i = 0
    n = len(encoded_seqs)
    s = encoded_seqs

    repeated = set()

    # ABCABCDD
    # `i
    # +-+w
    #    `j
    # slide the window for comparing the current i and next window j
    
    while i < n:
        for w in range(min_w, max_w+1):
            j = i + w
            while j < n:
                s1 = tuple(s[i:w+i])
                s2 = tuple(s[j:w+j])
                                
                if s1 == s2:
                    repeated.add(s1)
                j += 1
        i += 1

    out = repeated
    return out

# Try all combinations of subseqs.
# For each subseq, match the prefix of the current sequence,
# and try all subseqs for the remaining sequence recursively
def compress_seq(encoded_seqs, all_subseqs, path):

    if len(encoded_seqs) == 0:
        return path

    all_paths = []
    for s in all_subseqs:
        sn = len(s)
        if encoded_seqs[0:sn] == list(s):
            new_path = path.copy()
            new_path.append(s)
            p = compress_seq(encoded_seqs[sn::], all_subseqs, new_path)
            if p:
                all_paths.append(p)

    # For the question, we can only have 3 types of robot function,
    # so the used type of subseqs needed to be 3
    out_p = None
    diff_subseqs = None
    for p in all_paths:
        if p:
            diff_subseqs = set(p)
            if len(diff_subseqs) == 3:
                out_p = p
            
    return out_p

# Similar way to try all combinations of subseqs.
# But try to find the shotest combinations
def compress_seq_shorter(encoded_seqs, all_subseqs, path):

    if len(encoded_seqs) == 0:
        return path

    all_paths = []
    for s in all_subseqs:
        sn = len(s)
        if encoded_seqs[0:sn] == list(s):
            new_path = path.copy()
            new_path.append(s)
            p = compress_seq(encoded_seqs[sn::], all_subseqs, new_path)
            if p:
                all_paths.append(p)

    pl = 99999999
    out_p = None
    for p in all_paths:
        if p and len(p) < pl:
            pl = len(p)
            out_p = p
            
    return out_p

def compressed_seq_to_bot_functions(compressed_seq):
    f = ord('a') # compatiable with my older code
    d = {}
    for c in compressed_seq:
        if "".join(c) not in d.values():
            d[chr(f)] = "".join(c) # old code is using string for subseq
            f += 1

    return d


### ---- end of robot rubroutine compression algor
                    

def main():
    with open(sys.argv[1], 'r') as fd:
        prog = fd.readline().strip().split(',')

    prog_ex = [0] * (len(prog) * 10)
    prog.extend(prog_ex)

    cam = Cam(prog.copy())
    cam.build_image()
    print("----")
    cam.print_image()

    inters = cam.find_intersections()
    print("Intersecations (x,y):", inters)
    print("----")
    cam.print_image()
    print("Part 1 ans:", part1_ans(inters))


    cam.build_vmap()
    cam.print_vmap()

    print(cam.get_patch(17, 1))
    print(cam.get_patch(8+1, 12+1)) # first intersection, added the v_map offset

    vertices = cam.find_vertices()
    cam.print_vmap(marks=vertices, ascii_code=ord('X'))
    print("vertices (x,y):", vertices)

    corners = cam.find_corners()
    cam.print_vmap(marks=corners, ascii_code=ord('+'))
    print("corners (x,y):", corners)


    graph = Graph(cam.v_map, vertices, corners)
    edges = graph.find_edges(cam.v_map, ord('#'))

#    graph.print_edges()

    # Trace the path
    inters = trans_image_intersections_to_vmap(inters)
    seqs = graph.path_follow(cam.v_map_start, Graph.FACE_DIR_N, inters)

    list(map(print, seqs))

    # Encode the step sequences to char list for human friendiness
    (encoded_seqs, seq_rev_d) = trans_seqs_to_str_list(seqs)
    print("Encoded sequences")
    print(encoded_seqs)

    lrs = longest_repeated_substring(encoded_seqs)
    print(lrs)

    seq_map_dict = {}
    e = group_seq_str_with_lrs(encoded_seqs, lrs, ord('a'), seq_map_dict)
    print(e)

    t = ['A', 'B', 'C', 'A', 'B', 'C', 'D', 'A', 'A', 'D', 'D', 'A', 'A', 'D']

    r = all_repeated_substr(t, 3, 5)
    print(r)

    r = all_repeated_substr(encoded_seqs, 3, 5)
    print(r)

    r = repeated_subseqs(encoded_seqs, 3, 5)
    print("All repeated subseqs:")
    print(r)

    p = compress_seq(encoded_seqs, r, [])
    print("Compressed subseqs:")
    print(p)

    ps = compress_seq_shorter(encoded_seqs, r, [])
    print("Compressed subseqs using shortest combination algor:")
    print(ps)

    bot_functions = compressed_seq_to_bot_functions(p)
    print("The bot function to subseq mapping:")
    print(bot_functions)
    
    # XXX
    # Have to read ecnoded_seqs and group them
    # as sub functions manually. Any good algorithem to slove this?
    # XXXX Check above, using the brute force to find all subseqs combinations
    # for bot_functions
#    bot_functions = {
#        "a": "ABC",
#        "b": "DAADD",
#        "c": "BAEE"
#        }

    bot_func_seq = trans_bot_funs_to_seq(bot_functions, seq_rev_d)
    print(bot_func_seq)

    bot_main = trans_seq_to_bot_main_fun(encoded_seqs, bot_functions)
    print("MAIN")
    print(bot_main)

    bot_input = gen_robot_input(bot_main, bot_func_seq)

    # Cannot reuse part1 prog
    p2_prog = prog.copy()
    p2_rc = intcode.IntCodeRunConfig()
    p2_rc.in_buf = queue.Queue()
    p2_rc.out_buf = queue.Queue()
    p2_rc.no_head = True
    p2_rc.use_fd = False

    p2_prog[0] = "2"
    input_cnt = 0
    pc = intcode.run(p2_prog, p2_rc, 0)
    opcode = intcode.get_opcode_from_pc(p2_prog, pc)
    print("BOT INPUT", bot_input)
    lost_bot = False
    got_ans = False
    big_num_ans = 0
    while opcode != 99 and not lost_bot and not got_ans:
        while p2_rc.out_buf.qsize() > 0:
            i = int(p2_rc.out_buf.get())
            print(chr(i), end="")
            if chr(i) == 'X':
                print("Lost bot!")
                lost_bot = True
            if i > 255:
                got_ans = True
                big_num_ans = i

        print("")

        if lost_bot or got_ans:
            break
        
        if opcode == 3 and input_cnt < len(bot_input):
            p2_rc.in_buf.put(bot_input[input_cnt])
            input_cnt += 1
            
        pc = intcode.run(p2_prog, p2_rc, pc)


    print("Got space dust:", big_num_ans)

    return 0

        


if __name__ == "__main__":
    sys.exit(main())
